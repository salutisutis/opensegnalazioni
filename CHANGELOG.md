# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.5.6](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.5...2.5.6) - 2021-05-05


#### Code dependencies
| Changes                         | From    | To        | Compare                                                                            |
|---------------------------------|---------|-----------|------------------------------------------------------------------------------------|
| aws/aws-sdk-php                 | 3.179.2 | 3.180.2   | [...](https://github.com/aws/aws-sdk-php/compare/3.179.2...3.180.2)                |
| composer/installers             | 207a91f | v1.0.25   | [...](https://github.com/composer/installers/compare/207a91f...v1.0.25)            |
| opencontent/ocopendata_forms-ls | 1.6.10  | 1.6.11    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.10...1.6.11) |
| opencontent/ocsensorapi         | 5.6.2   | 5.6.3     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.2...5.6.3)        |
| predis/predis                   | d72f067 | 1d7ccb6   | [...](https://github.com/predis/predis/compare/d72f067...1d7ccb6)                  |
| psr/container                   | 381524e | 1.1.x-dev | [...](https://github.com/php-fig/container/compare/381524e...1.1.x-dev)            |


Relevant changes by repository:

**[opencontent/ocopendata_forms-ls changes between 1.6.10 and 1.6.11](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.10...1.6.11)**
* Corregge il valore di default del campo BooleanField in accordo con i parametri dell'attributo di classe

**[opencontent/ocsensorapi changes between 5.6.2 and 5.6.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.2...5.6.3)**
* Splits the PostAging 7-30 bucket in 7-15 and 15-30
* Allow api post embed fields Refactor serializer


## [2.5.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.4...2.5.5) - 2021-05-04
- Update ezpublish to 2020.1000.6

#### Code dependencies
| Changes                    | From        | To          | Compare                                                                                  |
|----------------------------|-------------|-------------|------------------------------------------------------------------------------------------|
| ezsystems/ezpublish-legacy | 2020.1000.4 | 2020.1000.6 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.4...2020.1000.6) |
| psr/log                    | a18c1e6     | d49695b     | [...](https://github.com/php-fig/log/compare/a18c1e6...d49695b)                          |



## [2.5.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.3...2.5.4) - 2021-05-01
- Fix changelog links

#### Code dependencies
| Changes                   | From    | To      | Compare                                                                      |
|---------------------------|---------|---------|------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.179.1 | 3.179.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.179.1...3.179.2)          |
| opencontent/ocopendata-ls | 2.25.3  | 2.25.4  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.3...2.25.4) |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.25.3 and 2.25.4](https://github.com/OpencontentCoop/ocopendata/compare/2.25.3...2.25.4)**
* Force la rigenerazione delle cache delle classi per evitare un fatal error


## [2.5.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.2...2.5.3) - 2021-04-30
- Update php e nginx base images to 1.2.1

#### Code dependencies
| Changes                 | From  | To    | Compare                                                                     |
|-------------------------|-------|-------|-----------------------------------------------------------------------------|
| opencontent/ocsensorapi | 5.6.1 | 5.6.2 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.1...5.6.2) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.6.1 and 5.6.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.1...5.6.2)**
* Fix typo in trend stat


## [2.5.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.1...2.5.2) - 2021-04-30


#### Code dependencies
| Changes                   | From    | To      | Compare                                                                       |
|---------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.178.7 | 3.179.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.178.7...3.179.1)           |
| opencontent/ocsensor-ls   | 5.9.1   | 5.9.2   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.9.1...5.9.2)      |
| opencontent/ocsensorapi   | 5.6.0   | 5.6.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.0...5.6.1)   |
| symfony/polyfill-mbstring | 298b87c | 9ad2f3c | [...](https://github.com/symfony/polyfill-mbstring/compare/298b87c...9ad2f3c) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.9.1 and 5.9.2](https://github.com/OpencontentCoop/ocsensor/compare/5.9.1...5.9.2)**
* Introduce una configurazione ocsensor.ini per permettere la modifica del riferimento da gui
* Corregge l'export in csv di aree e categorie
* Visualizza un filtro per macrocategoria per le faq
* Introduce l'invio di una mail di benvenuto per gli utenti creati da operatore o via api

**[opencontent/ocsensorapi changes between 5.6.0 and 5.6.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.6.0...5.6.1)**
* Send OnAddApproverNotification also to users
* Fix stat trend title
* Fire on_generate_user event when user is created from api or operator Add WelcomeUserListener fired on on_generate_user event


## [2.5.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.5.0...2.5.1) - 2021-04-22
- Force nginx client_max_body_size


#### Installer
- Update version to 2.4.3

#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.9.0 | 5.9.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.9.0...5.9.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.9.0 and 5.9.1](https://github.com/OpencontentCoop/ocsensor/compare/5.9.0...5.9.1)**
* Corregge un bug per cui non era possibile esportare correttamente le segnalazioni in csv


## [2.5.0](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.8...2.5.0) - 2021-04-22



#### Installer
- Add faq class and container

#### Code dependencies
| Changes                          | From    | To      | Compare                                                                              |
|----------------------------------|---------|---------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.178.2 | 3.178.7 | [...](https://github.com/aws/aws-sdk-php/compare/3.178.2...3.178.7)                  |
| firebase/php-jwt                 |         | v5.2.1  |                                                                                      |
| opencontent/ocinstaller          | e701855 | 95b1463 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/e701855...95b1463)      |
| opencontent/ocopendata-ls        | 2.25.2  | 2.25.3  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.2...2.25.3)         |
| opencontent/ocsensor-ls          | 5.8.0   | 5.9.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.8.0...5.9.0)             |
| opencontent/ocsensorapi          | 5.5.1   | 5.6.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.1...5.6.0)          |
| symfony/polyfill-ctype           | c6c942b | 46cd957 | [...](https://github.com/symfony/polyfill-ctype/compare/c6c942b...46cd957)           |
| symfony/polyfill-intl-idn        | 3709eb8 | 780e280 | [...](https://github.com/symfony/polyfill-intl-idn/compare/3709eb8...780e280)        |
| symfony/polyfill-intl-normalizer | 43a0283 | 8590a5f | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/43a0283...8590a5f) |
| symfony/polyfill-mbstring        | 5232de9 | 298b87c | [...](https://github.com/symfony/polyfill-mbstring/compare/5232de9...298b87c)        |
| symfony/polyfill-php72           | cc6e6f9 | 95695b8 | [...](https://github.com/symfony/polyfill-php72/compare/cc6e6f9...95695b8)           |
| symfony/polyfill-php73           | a678b42 | fba8933 | [...](https://github.com/symfony/polyfill-php73/compare/a678b42...fba8933)           |
| symfony/polyfill-php80           | dc3063b | eca0bf4 | [...](https://github.com/symfony/polyfill-php80/compare/dc3063b...eca0bf4)           |


Relevant changes by repository:

**[opencontent/ocinstaller changes between e701855 and 95b1463](https://github.com/OpencontentCoop/ocinstaller/compare/e701855...95b1463)**
* Add load_policies param in role step (se false to avoid role regeneration)
* avoid warning in IOTools

**[opencontent/ocopendata-ls changes between 2.25.2 and 2.25.3](https://github.com/OpencontentCoop/ocopendata/compare/2.25.2...2.25.3)**
* Evita un possibile errore nel calcolo della paginazione a cursore

**[opencontent/ocsensor-ls changes between 5.8.0 and 5.9.0](https://github.com/OpencontentCoop/ocsensor/compare/5.8.0...5.9.0)**
* Introduce il metodo di autenticazione alle api con Bearer JWT
* Rimuove il bottone csv export dalle categorie e lo introduce negli automatismi Corregge alcuni bug minori
* Introduce un'interfaccia per la visualizzazione e la modifica delle faq
* Aggiunto il numero di risposte e di note nell'export csv delle segnalazioni
* Inserisce i claim di hasura nella generazione del token JWT
* Corregge una vulnerabilità di tipo user enumeration nel modulo di recupero password
* Introduce il selettore delle categorie nell'interfaccia di pubblicazione ajax Introduce le chiavi meta persistenti PersistentMetaKeys per impedire la sovrascrittura di campi meta in modifica della segnalazione
* Introduce la possibilità di modificare la tipologia di segnalazione
* Permette al browser di usare la cache per le chiamate alle immagini avatar Introduce gli avatar nei settings di gruppi e operatori
* Merge branch 'development' of https://github.com/OpencontentCoop/ocsensor into development
* Introduce il grafico del trend apertura/chiusura Introduce uno script di reindicizzazione dei soli sensor_post Corregge la lingua utilizzata da Highcharts
* Introduce uno script per modificare la proprietà delle segnalazioni da un utente a un altro

**[opencontent/ocsensorapi changes between 5.5.1 and 5.6.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.1...5.6.0)**
* Rename UnauthorizedException in ForbiddenException
* Add auth openapi doc if SensorJwtManager is enabled
* Minor bugfixes
* Add faq service
* Remove deprecated CategoryAutomaticAssignListener and all operator relations in category and area entities (using scenarios instead)
* Show operators in owner_groups stat filtered by group
* Add application/vnd.geo+json media type in api GET /posts and boost geojson post search
* Add api user/current GET and PUT
* Build api post doc based on the sensor_post class specification
* Add SetType action and CanSetType permission
* Add api category.parent Fix api search parameters
* Add first assignment and closing date parser in SolrMapper Add trend stat


## [2.4.8](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.7...2.4.8) - 2021-04-13



#### Installer
- Update installer version
- Remove stat access policies from roles
- Add sensor_group/tag field

#### Code dependencies
| Changes                      | From    | To      | Compare                                                                         |
|------------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.176.5 | 3.178.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.176.5...3.178.2)             |
| opencontent/ocbootstrap-ls   | 1.10.3  | 1.10.5  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.3...1.10.5)   |
| opencontent/ocinstaller      | 995b7bc | e701855 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/995b7bc...e701855) |
| opencontent/ocmultibinary-ls | 2.3.0   | 2.3.1   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.3.0...2.3.1)   |
| opencontent/ocsensor-ls      | 5.7.1   | 5.8.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.7.1...5.8.0)        |
| opencontent/ocsensorapi      | 5.5.0   | 5.5.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.0...5.5.1)     |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.3 and 1.10.5](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.3...1.10.5)**
* Aggiorna le traduzioni delle stringhe
* Protegge con CSRF token la chiamata post ajax di ocevents

**[opencontent/ocinstaller changes between 995b7bc and e701855](https://github.com/OpencontentCoop/ocinstaller/compare/995b7bc...e701855)**
* Add rename_tag step
* Fix rename_tag step

**[opencontent/ocmultibinary-ls changes between 2.3.0 and 2.3.1](https://github.com/OpencontentCoop/ocmultibinary/compare/2.3.0...2.3.1)**
* Add CSRF token in ajax post

**[opencontent/ocsensor-ls changes between 5.7.1 and 5.8.0](https://github.com/OpencontentCoop/ocsensor/compare/5.7.1...5.8.0)**
* Aggiunge il token csrf se configurato alle richieste post in ajax
* Introduce un'interfaccia di configurazione per abilitare l'accesso alle statistiche
* Introduce un nuovo campo tag per raggruppare i gruppi nelle statistiche Visualizza il filtro per macro categorie sui grafici di stato e di tipo per categoria

**[opencontent/ocsensorapi changes between 5.5.0 and 5.5.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.5.0...5.5.1)**
* Add main category filter
* Add tag in group and tag group filter in stat
* Fix category stat filtered by main category


## [2.4.7](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.6...2.4.7) - 2021-04-08


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.7.0 | 5.7.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.7.0...5.7.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.7.0 and 5.7.1](https://github.com/OpencontentCoop/ocsensor/compare/5.7.0...5.7.1)**
* Evita un errore in fase di registrazione se il codice fiscale non è obbligatorio


## [2.4.6](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.5...2.4.6) - 2021-04-01



#### Installer
- Add sensor_post_root/additional_map_layers Add sensor_scenario/expiry Fix sensor_scenario labels

#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.174.3 | 3.176.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.174.3...3.176.5)                    |
| opencontent/ocopendata-ls          | 2.25.1  | 2.25.2  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.1...2.25.2)           |
| opencontent/ocsensor-ls            | 5.6.3   | 5.7.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.3...5.7.0)               |
| opencontent/ocsensorapi            | 5.4.4   | 5.5.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.4...5.5.0)            |
| symfony/deprecation-contracts      | 49dc45a | 5f38c88 | [...](https://github.com/symfony/deprecation-contracts/compare/49dc45a...5f38c88)      |
| symfony/event-dispatcher-contracts | 9b7cabf | 69fee1a | [...](https://github.com/symfony/event-dispatcher-contracts/compare/9b7cabf...69fee1a) |
| zetacomponents/persistent-object   | 1.8     | 1.8.1   | [...](https://github.com/zetacomponents/PersistentObject/compare/1.8...1.8.1)          |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.25.1 and 2.25.2](https://github.com/OpencontentCoop/ocopendata/compare/2.25.1...2.25.2)**
* Permette di specificate le preferenze di lingua in TagRepository

**[opencontent/ocsensor-ls changes between 5.6.3 and 5.7.0](https://github.com/OpencontentCoop/ocsensor/compare/5.6.3...5.7.0)**
* Permette di configurare i layer di mappa addizionali direttamente da applicazione (attraverso l'attributo sensor_post_root/additional_map_layers)
* Introduce il grafico "Tipologia per categoria"
* Permette di configurare la scadenza della segnalazione negli automatismi

**[opencontent/ocsensorapi changes between 5.4.4 and 5.5.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.4...5.5.0)**
* Allow set post expiration in scenario
* Set scenario owner_group in category tree item group. Clear category tree cache on create/edit scenario
* Add type_categories stat


## [2.4.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.4...2.4.5) - 2021-03-19


#### Code dependencies
| Changes                      | From     | To      | Compare                                                                       |
|------------------------------|----------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.173.27 | 3.174.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.27...3.174.3)          |
| opencontent/ocsensor-ls      | 5.6.2    | 5.6.3   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.2...5.6.3)      |
| opencontent/ocsensorapi      | 5.4.3    | 5.4.4   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.3...5.4.4)   |
| opencontent/openpa_sensor-ls | 4.5.0    | 4.5.1   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.0...4.5.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.6.2 and 5.6.3](https://github.com/OpencontentCoop/ocsensor/compare/5.6.2...5.6.3)**
* Nasconde la registrazione utente in caso di login template attivato
* Corregge i parametri di richiesta del range di date nei grafici
* Introduce il grafico 'Aperte per gruppo di incaricati'
* Corregge le visualizzazioni dei grafici
* Introduce una visualizzazione della cronologia della segnalazione ad uso degli amministratori
* Evita di produrre un warning nel modulo sensor/posts
* Visualizza sempre l'ultimo operatore/gruppo di operatori incaricato
* Considera i permessi nell'esportazione in csv e aggiunge il campo risposta al csv.
* Corregge un bug sulla paginazione del download csv
* Considera il filtro per gruppo di incaricati nell'export in csv
* Introduce un permesso dedicato per accedere al modulo sensor/user

**[opencontent/ocsensorapi changes between 5.4.3 and 5.4.4](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.3...5.4.4)**
* Fix stat range filter Add OpenPerOwnerGroup as StatusPerOwnerGroup extension
* Fix stat range filter for field published
* Add audit on send notifications
* Remove debug code comment
* Add latestOwner and latestOwnerGroup post fields
* Allow empty query in search service

**[opencontent/openpa_sensor-ls changes between 4.5.0 and 4.5.1](https://github.com/OpencontentCoop/openpa_sensor/compare/4.5.0...4.5.1)**
* Corregge il controllo dei permessi per visualizzare il menu utenti


## [2.4.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.3...2.4.4) - 2021-03-11
- Hotfix remove debug settings



## [2.4.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.2...2.4.3) - 2021-03-11


#### Code dependencies
| Changes                            | From     | To       | Compare                                                                             |
|------------------------------------|----------|----------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.173.25 | 3.173.27 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.25...3.173.27)               |
| opencontent/ezpostgresqlcluster-ls | f37c194  | ef754ff  | [...](https://github.com/Opencontent/ezpostgresqlcluster/compare/f37c194...ef754ff) |
| opencontent/ocsearchtools-ls       | 1.11.0   | 1.11.1   | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.0...1.11.1)     |
| opencontent/ocsensor-ls            | 5.6.1    | 5.6.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.1...5.6.2)            |
| opencontent/ocsensorapi            | 5.4.1    | 5.4.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.1...5.4.3)         |


Relevant changes by repository:

**[opencontent/ezpostgresqlcluster-ls changes between f37c194 and ef754ff](https://github.com/Opencontent/ezpostgresqlcluster/compare/f37c194...ef754ff)**
* Add static method storeClusterizedFileMetadata for migrations

**[opencontent/ocsearchtools-ls changes between 1.11.0 and 1.11.1](https://github.com/OpencontentCoop/ocsearchtools/compare/1.11.0...1.11.1)**
* Evita un errore nella sincronizzazione dei gruppi di classe

**[opencontent/ocsensor-ls changes between 5.6.1 and 5.6.2](https://github.com/OpencontentCoop/ocsensor/compare/5.6.1...5.6.2)**
* Aggiunge il filtro per gruppo nelle statistiche e corregge alcuni errori sui filtri di ricerca

**[opencontent/ocsensorapi changes between 5.4.1 and 5.4.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.1...5.4.3)**
* Count StatusPerOwnerGroup per last_owner instead of history_owner
* Add owner group filter in stats


## [2.4.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.1...2.4.2) - 2021-03-10
- Update to traefik2 Add openlogin and spid test Remove varnish

#### Code dependencies
| Changes                    | From        | To          | Compare                                                                                  |
|----------------------------|-------------|-------------|------------------------------------------------------------------------------------------|
| aws/aws-sdk-php            | 3.173.21    | 3.173.25    | [...](https://github.com/aws/aws-sdk-php/compare/3.173.21...3.173.25)                    |
| ezsystems/ezpublish-legacy | 2020.1000.3 | 2020.1000.4 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.3...2020.1000.4) |
| guzzlehttp/promises        | ddfeedf     | 8e7d04f     | [...](https://github.com/guzzle/promises/compare/ddfeedf...8e7d04f)                      |
| opencontent/ocinstaller    | ae76fef     | 995b7bc     | [...](https://github.com/OpencontentCoop/ocinstaller/compare/ae76fef...995b7bc)          |
| opencontent/ocsensor-ls    | 5.6.0       | 5.6.1       | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.6.0...5.6.1)                 |
| opencontent/ocsensorapi    | 5.4.0       | 5.4.1       | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.0...5.4.1)              |
| opencontent/ocsiracauth-ls | 1.0         | 1.1         | [...](https://github.com/OpencontentCoop/ocsiracauth/compare/1.0...1.1)                  |
| opencontent/ocsupport-ls   | 54e52b0     | 8071c61     | [...](https://github.com/OpencontentCoop/ocsupport/compare/54e52b0...8071c61)            |


Relevant changes by repository:

**[opencontent/ocinstaller changes between ae76fef and 995b7bc](https://github.com/OpencontentCoop/ocinstaller/compare/ae76fef...995b7bc)**
* Allow deprecate topic without replacement
* Avoid error in DeprecateTopic installer
* Fix pagination in DepracateTopic
* Move node before remove location avoiding block inconsistency

**[opencontent/ocsensor-ls changes between 5.6.0 and 5.6.1](https://github.com/OpencontentCoop/ocsensor/compare/5.6.0...5.6.1)**
* Migliora l'interfaccia e il flusso di controllo della registrazione utenti
* Introduce uno script per rimuovere le registrazioni bloccate
* Aggiunge il filtro sullo stato della segnalazione nella pagina segnalazioni
* Aggiunge il totale dei risultati della ricerca nella pagina segnalazioni
* Introduce il grafico post aging
* Filtra le notifiche attivabili in base ai permessi dell'utente corrente
* Migliora la navigazione delle pagine di configurazione

**[opencontent/ocsensorapi changes between 5.4.0 and 5.4.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.4.0...5.4.1)**
* Fix auto assign timeline message
* Force DateTimeZone in DateTime constructors
* Change notification group to operator in OnAddCommentToModerateNotificationType and OnSendPrivateMessageNotificationType
* Add last_owner_user_id and last_owner_group_id fields in SolrMapper
* Add PostAging stat
* Force status privacy in PostInitializer if HidePrivacyChoice setting is enabled

**[opencontent/ocsiracauth-ls changes between 1.0 and 1.1](https://github.com/OpencontentCoop/ocsiracauth/compare/1.0...1.1)**
* Migliora la gestione degli utenti già presenti a sistema Introduce un handler dedicato a openlogin

**[opencontent/ocsupport-ls changes between 54e52b0 and 8071c61](https://github.com/OpencontentCoop/ocsupport/compare/54e52b0...8071c61)**
* Fix tag listing tool
* Bugifx in tag list


## [2.4.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.4.0...2.4.1) - 2021-03-04


#### Code dependencies
| Changes                   | From     | To       | Compare                                                                         |
|---------------------------|----------|----------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.173.19 | 3.173.21 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.19...3.173.21)           |
| opencontent/ocinstaller   | 306ea1c  | ae76fef  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/306ea1c...ae76fef) |
| opencontent/ocopendata-ls | 2.25.0   | 2.25.1   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.25.0...2.25.1)    |
| opencontent/ocsensor-ls   | 5.5.0    | 5.6.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.5.0...5.6.0)        |
| opencontent/ocsensorapi   | 5.3.0    | 5.4.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.3.0...5.4.0)     |
| php-http/guzzle6-adapter  | 6f108cf  | 9d1a45e  | [...](https://github.com/php-http/guzzle6-adapter/compare/6f108cf...9d1a45e)    |
| psr/log                   | dd738d0  | a18c1e6  | [...](https://github.com/php-fig/log/compare/dd738d0...a18c1e6)                 |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 306ea1c and ae76fef](https://github.com/OpencontentCoop/ocinstaller/compare/306ea1c...ae76fef)**
* Add deprecate_topic installer step

**[opencontent/ocopendata-ls changes between 2.25.0 and 2.25.1](https://github.com/OpencontentCoop/ocopendata/compare/2.25.0...2.25.1)**
* Corregge un bug nella validazione api delle relazioni

**[opencontent/ocsensor-ls changes between 5.5.0 and 5.6.0](https://github.com/OpencontentCoop/ocsensor/compare/5.5.0...5.6.0)**
* Introduce il grafico di Pareto nel grafico Stato per categoria Aggiorna la libreria highchart
* Introduce il grafico di stato per gruppo di incaricati
* Migliora le performance di caricamento
* Corregge un bug per cui non veniva inviata la notifica alla creazione di una segnalazione
* Introduce la barra di ricerca avanzata per gli operatori sulla pagina segnalazioni
* Aggiunge uno script cli per rimuovere gli osservatori da una segnalazione

**[opencontent/ocsensorapi changes between 5.3.0 and 5.4.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.3.0...5.4.0)**
* Reorder data in StatusPerCategory stat
* Add StatusPerOwnerGroup stat
* Boost repository and search service performance
* Fix SendMailListener on on_create event
* Add owner_group_id_list in SolrMapper


## [2.4.0](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.5...2.4.0) - 2021-03-02
- Update php and nginx docker base image (adding healtcheck)
- Fix minio createbucket command
- Fix AssignRole in browse.ini
- Add changelog
- Updated list of municipalities, latest release date and number and links to external resources using short-urls
- Change base image with php7.2.33-clean
- Fix category access in sensor anonymous role

#### Code dependencies
| Changes                            | From    | To       | Compare                                                                                |
|------------------------------------|---------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.173.7 | 3.173.19 | [...](https://github.com/aws/aws-sdk-php/compare/3.173.7...3.173.19)                   |
| opencontent/ocopendata_forms-ls    | 1.6.9   | 1.6.10   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.9...1.6.10)      |
| opencontent/ocsensor-ls            | 5.4.2   | 5.5.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.4.2...5.5.0)               |
| opencontent/ocsensorapi            | 5.2.1   | 5.3.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.1...5.3.0)            |
| opencontent/ocsupport-ls           | 2ee7846 | 54e52b0  | [...](https://github.com/OpencontentCoop/ocsupport/compare/2ee7846...54e52b0)          |
| opencontent/ocwebhookserver-ls     | 67a56b7 | a60d571  | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/67a56b7...a60d571)    |
| opencontent/openpa_sensor-ls       | 4.4.0   | 4.5.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.4.0...4.5.0)          |
| symfony/deprecation-contracts      | d940483 | 49dc45a  | [...](https://github.com/symfony/deprecation-contracts/compare/d940483...49dc45a)      |
| symfony/event-dispatcher-contracts | 5e8ae4d | 9b7cabf  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5e8ae4d...9b7cabf) |
| symfony/polyfill-intl-idn          | 0eb8293 | 3709eb8  | [...](https://github.com/symfony/polyfill-intl-idn/compare/0eb8293...3709eb8)          |
| symfony/polyfill-intl-normalizer   | 6e971c8 | 43a0283  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/6e971c8...43a0283)   |
| symfony/polyfill-mbstring          | f377a3d | 5232de9  | [...](https://github.com/symfony/polyfill-mbstring/compare/f377a3d...5232de9)          |


Relevant changes by repository:

**[opencontent/ocopendata_forms-ls changes between 1.6.9 and 1.6.10](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.9...1.6.10)**
* Corregge alcuni bug minori
* Corregge un bug nella ricerca del punto sulla mappa nel contesto di un form dinamico

**[opencontent/ocsensor-ls changes between 5.4.2 and 5.5.0](https://github.com/OpencontentCoop/ocsensor/compare/5.4.2...5.5.0)**
* Introduce gli scenari (automatismi) per organizzare in maniera organica e flessibile le assegnazioni automatiche
* Invia una mail di benvenuto al nuovo operatore inserito
* Corregge il testo della mail di benvenuto all'operatore
* Corregge alcuni bug minori
* Aggiunge un connettore custom per gli scenari
* Visualizza i messaggi di audit
* Corregge un bug del custom payload serializer
* Utilizza i filtri impostati nell'interfaccia delle statistiche anche nell'esportazione in csv

**[opencontent/ocsensorapi changes between 5.2.1 and 5.3.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.1...5.3.0)**
* Add WelcomeOperatorListener on event on_new_operator
* Introducing ScenarioService
* Add scenario criterion description Fix scenario search limit
* Always trigger on_add_category event
* Add audit messages
* Hide operator name in public comment if needed
* Remove author from on_add_comment_to_moderate notification
* Fix stat titles

**[opencontent/ocsupport-ls changes between 2ee7846 and 54e52b0](https://github.com/OpencontentCoop/ocsupport/compare/2ee7846...54e52b0)**
* Add changelog tools
* changelog bugfixes

**[opencontent/ocwebhookserver-ls changes between 67a56b7 and a60d571](https://github.com/OpencontentCoop/ocwebhookserver/compare/67a56b7...a60d571)**
* Fix custom payload check in webhook edit

**[opencontent/openpa_sensor-ls changes between 4.4.0 and 4.5.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.4.0...4.5.0)**
* Aggiunto evento in post publish alla creazione di un nuovo operatore


## [2.3.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.4...2.3.5) - 2021-02-12


#### Code dependencies
| Changes                           | From     | To       | Compare                                                                                |
|-----------------------------------|----------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                   | 3.173.3  | 3.173.7  | [...](https://github.com/aws/aws-sdk-php/compare/3.173.3...3.173.7)                    |
| ezsystems/ezmultiupload-ls        | v5.3.1.2 | v5.3.1.3 | [...](https://github.com/ezsystems/ezmultiupload/compare/v5.3.1.2...v5.3.1.3)          |
| opencontent/ocmap-ls              | 93c8432  | 9bc1f0a  | [...](https://github.com/OpencontentCoop/ocmap/compare/93c8432...9bc1f0a)              |
| opencontent/ocsearchtools-ls      | 1.10.9   | 1.11.0   | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.9...1.11.0)        |
| opencontent/ocsensor-ls           | 5.4.1    | 5.4.2    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.4.1...5.4.2)               |
| opencontent/ocsensorapi           | 5.2.0    | 5.2.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.0...5.2.1)            |
| opencontent/openpa-ls             | 4f20fd8  | c68a756  | [...](https://github.com/OpencontentCoop/openpa/compare/4f20fd8...c68a756)             |
| opencontent/openpa_userprofile-ls | 4c7a67e  | 3998da2  | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/4c7a67e...3998da2) |
| psr/event-dispatcher              | 3ef040d  | aa4f89e  | [...](https://github.com/php-fig/event-dispatcher/compare/3ef040d...aa4f89e)           |


Relevant changes by repository:

**[opencontent/ocmap-ls changes between 93c8432 and 9bc1f0a](https://github.com/OpencontentCoop/ocmap/compare/93c8432...9bc1f0a)**
* Corregge un bug che emetteva un warning in caso di attributo vuoto

**[opencontent/ocsearchtools-ls changes between 1.10.9 and 1.11.0](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.9...1.11.0)**
* Rimuove di default le cache calendartaxonomy e calendarquery che vanno attivate qualora di utilizzino le funzionalità

**[opencontent/ocsensor-ls changes between 5.4.1 and 5.4.2](https://github.com/OpencontentCoop/ocsensor/compare/5.4.1...5.4.2)**
* Aggiunge il grafico di stato per categoria
* Aggiunge un grafico a torta su categorie e zone
* Corregge la logica di inbox per l'operatore e il permesso di accesso al modulo user
* Nasconde il nome degli operatori nelle notifiche in caso di flag HideOperatorNames attivato

**[opencontent/ocsensorapi changes between 5.2.0 and 5.2.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.2.0...5.2.1)**
* Add StatusPerCategory statistic
* Sort Users stat data
* Fix StatusPerCategory stat

**[opencontent/openpa-ls changes between 4f20fd8 and c68a756](https://github.com/OpencontentCoop/openpa/compare/4f20fd8...c68a756)**
* Minor bugfix

**[opencontent/openpa_userprofile-ls changes between 4c7a67e and 3998da2](https://github.com/OpencontentCoop/openpa_userprofile/compare/4c7a67e...3998da2)**
* Corregge una vulnerabilità XSS


## [2.3.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.3...2.3.4) - 2021-02-05


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 5.4.0 | 5.4.1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.4.0...5.4.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.4.0 and 5.4.1](https://github.com/OpencontentCoop/ocsensor/compare/5.4.0...5.4.1)**
* Corregge un bug per cui nell'inbox non venivano correttamente visualizzati i titoli dei widget


## [2.3.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.2...2.3.3) - 2021-02-05


#### Code dependencies
| Changes                        | From        | To          | Compare                                                                                  |
|--------------------------------|-------------|-------------|------------------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.172.2     | 3.173.3     | [...](https://github.com/aws/aws-sdk-php/compare/3.172.2...3.173.3)                      |
| ezsystems/ezpublish-legacy     | 2020.1000.1 | 2020.1000.3 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/2020.1000.1...2020.1000.3) |
| opencontent/occodicefiscale-ls | 2d040a8     | 24253b0     | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/2d040a8...24253b0)      |
| opencontent/ocrecaptcha-ls     | 1.4         | 1.5         | [...](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.4...1.5)                  |
| opencontent/ocsensor-ls        | 5.3.0       | 5.4.0       | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.3.0...5.4.0)                 |
| opencontent/ocsensorapi        | 5.1.0       | 5.2.0       | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.1.0...5.2.0)              |
| opencontent/ocwebhookserver-ls | aabd35a     | 67a56b7     | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/aabd35a...67a56b7)      |
| opencontent/openpa_sensor-ls   | 4.3.0       | 4.4.0       | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.3.0...4.4.0)            |
| php-http/message               | c6f875f     | 1.11.0      | [...](https://github.com/php-http/message/compare/c6f875f...1.11.0)                      |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between 2d040a8 and 24253b0](https://github.com/OpencontentCoop/occodicefiscale/compare/2d040a8...24253b0)**
* Corregge un bug nel connettore di ocdescriptionboolean
* Aggiunge la validazione del codice fiscale nel connettore opendata

**[opencontent/ocrecaptcha-ls changes between 1.4 and 1.5](https://github.com/OpencontentCoop/ocrecaptcha/compare/1.4...1.5)**
* Verifica la presenza del valore post in validateObjectAttributeHTTPInput
* Verifica la presenza del valore post in validateCollectionAttributeHTTPInput
* Disabilita il connettore recaptcha se l'utente corrente è autenticato

**[opencontent/ocsensor-ls changes between 5.3.0 and 5.4.0](https://github.com/OpencontentCoop/ocsensor/compare/5.3.0...5.4.0)**
* Corregge un problema dell'esportatore dei csv e un errore del form di inserimento segnalazione
* Corregge alcuni bug minori sulle api e sui template
* Abilita l'interfaccia di rifiuto di un commento da moderare
* Introduce uno script per modificare massivamente la privacy delle segnalazioni
* Merge branch 'development' into reject-comment
* Corregge il caricamento dei font
* Corregge un bug nel connettore semplificato dello user account
* Introduce il modulo sensor/user ad uso degli operatori
* Impone l'utente anonimo come default in caso di segnalazione per conto di
* Corregge la visualizzazione dei controlli su mappa

**[opencontent/ocsensorapi changes between 5.1.0 and 5.2.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.1.0...5.2.0)**
* Add rejected comment flag
* Api bugfix
* Add author email in api schema
* Remove group as default target in observer notification
* Add user/phone field
* Refactor assign per category

**[opencontent/ocwebhookserver-ls changes between aabd35a and 67a56b7](https://github.com/OpencontentCoop/ocwebhookserver/compare/aabd35a...67a56b7)**
* Allow payload customizations via trigger configuration

**[opencontent/openpa_sensor-ls changes between 4.3.0 and 4.4.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.3.0...4.4.0)**
* Visualizza il menu UTENTI se abilitato


## [2.3.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.1...2.3.2) - 2021-01-28


#### Code dependencies
| Changes                          | From     | To      | Compare                                                                             |
|----------------------------------|----------|---------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.20 | 3.172.2 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.20...3.172.2)                |
| opencontent/ocbootstrap-ls       | 1.10.2   | 1.10.3  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.2...1.10.3)       |
| opencontent/ocsensor-ls          | 5.2.0    | 5.3.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.2.0...5.3.0)            |
| opencontent/ocsensorapi          | 5.0.3    | 5.1.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.3...5.1.0)         |
| opencontent/openpa-ls            | c036b88  | 4f20fd8 | [...](https://github.com/OpencontentCoop/openpa/compare/c036b88...4f20fd8)          |
| opencontent/openpa_theme_2014-ls | 2.15.3   | 2.15.4  | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.3...2.15.4) |
| php-http/message                 | d23ac28  | c6f875f | [...](https://github.com/php-http/message/compare/d23ac28...c6f875f)                |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.2 and 1.10.3](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.2...1.10.3)**
* Corregge una vulnerabilità XSS

**[opencontent/ocsensor-ls changes between 5.2.0 and 5.3.0](https://github.com/OpencontentCoop/ocsensor/compare/5.2.0...5.3.0)**
* Aggiunge un input di ricerca alla dashboard dedicata agli utenti
* Introduce la selezione di un range di date nei grafici
* Consente di visualizzare ulteriori layer WMS nelle mappe
* Corregge un bug nel sistema di esportazione in csv
* Corregge un problema per cui al clone di una segnalazione il canale veniva preselezionato al primo valore
* Rimuove la configurazione AdditionalMapLayers di test
* Rimuove il range di tempo per il filtro mensile
* Introduce il selettore dei layer nelle mappe

**[opencontent/ocsensorapi changes between 5.0.3 and 5.1.0](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.3...5.1.0)**
* Add stat range filter

**[opencontent/openpa-ls changes between c036b88 and 4f20fd8](https://github.com/OpencontentCoop/openpa/compare/c036b88...4f20fd8)**
* Add redis cluster connector verbose logs

**[opencontent/openpa_theme_2014-ls changes between 2.15.3 and 2.15.4](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.3...2.15.4)**
* Corregge una vulnerabilità XSS


## [2.3.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.27...2.3.1) - 2021-01-26
- Add optional socketio redis adapter

#### Code dependencies
| Changes                      | From     | To       | Compare                                                                       |
|------------------------------|----------|----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.171.19 | 3.171.20 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.19...3.171.20)         |
| opencontent/ocsensor-ls      | 5.1.1    | 5.2.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.1.1...5.2.0)      |
| opencontent/ocsensorapi      | 5.0.2    | 5.0.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)   |
| opencontent/openpa_sensor-ls | 4.2.5    | 4.3.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.5...4.3.0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.1.1 and 5.2.0](https://github.com/OpencontentCoop/ocsensor/compare/5.1.1...5.2.0)**
* Introduce un’interfaccia responsive di gestione delle segnalazioni ad uso dell’operatore attivabile in homepage o in menu. Richiede la configurazione di un server socket dedicato

**[opencontent/ocsensorapi changes between 5.0.2 and 5.0.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)**
* Fix some SolrMapper bug

**[opencontent/openpa_sensor-ls changes between 4.2.5 and 4.3.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.5...4.3.0)**
* Visualizza il menu INBOX se abilitato


## [2.2.27](https://gitlab.com/opencontent/opensegnalazioni/compare/2.3.0...2.2.27) - 2021-01-19


#### Code dependencies
| Changes                      | From     | To       | Compare                                                                       |
|------------------------------|----------|----------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.171.20 | 3.171.19 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.20...3.171.19)         |
| opencontent/ocsensor-ls      | 5.2.0    | 5.1.1    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.2.0...5.1.1)      |
| opencontent/ocsensorapi      | 5.0.3    | 5.0.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.3...5.0.2)   |
| opencontent/openpa_sensor-ls | 4.3.0    | 4.2.5    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.3.0...4.2.5) |



## [2.3.0](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.26...2.3.0) - 2021-01-18


#### Code dependencies
| Changes                          | From     | To       | Compare                                                                             |
|----------------------------------|----------|----------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.17 | 3.171.20 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.17...3.171.20)               |
| opencontent/ocfoshttpcache-ls    | faa918c  | 5ab0d91  | [...](https://github.com/OpencontentCoop/ocfoshttpcache/compare/faa918c...5ab0d91)  |
| opencontent/ocinstaller          | 7a4ec2b  | 306ea1c  | [...](https://github.com/OpencontentCoop/ocinstaller/compare/7a4ec2b...306ea1c)     |
| opencontent/ocsensor-ls          | 5.1.0    | 5.2.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.1.0...5.2.0)            |
| opencontent/ocsensorapi          | 5.0.2    | 5.0.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)         |
| opencontent/openpa_sensor-ls     | 4.2.4    | 4.3.0    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.4...4.3.0)       |
| opencontent/openpa_theme_2014-ls | 2.15.2   | 2.15.3   | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.2...2.15.3) |


Relevant changes by repository:

**[opencontent/ocfoshttpcache-ls changes between faa918c and 5ab0d91](https://github.com/OpencontentCoop/ocfoshttpcache/compare/faa918c...5ab0d91)**
* Minor bugfix

**[opencontent/ocinstaller changes between 7a4ec2b and 306ea1c](https://github.com/OpencontentCoop/ocinstaller/compare/7a4ec2b...306ea1c)**
* Minor fixes

**[opencontent/ocsensor-ls changes between 5.1.0 and 5.2.0](https://github.com/OpencontentCoop/ocsensor/compare/5.1.0...5.2.0)**
* Corregge un bug nella visualizzazione della mappa nella segnalazione
* Introduce un’interfaccia responsive di gestione delle segnalazioni ad uso dell’operatore attivabile in homepage o in menu. Richiede la configurazione di un server socket dedicato

**[opencontent/ocsensorapi changes between 5.0.2 and 5.0.3](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.2...5.0.3)**
* Fix some SolrMapper bug

**[opencontent/openpa_sensor-ls changes between 4.2.4 and 4.3.0](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.4...4.3.0)**
* Rifattorizza alcuni template di default
* Visualizza il menu INBOX se abilitato

**[opencontent/openpa_theme_2014-ls changes between 2.15.2 and 2.15.3](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.2...2.15.3)**
* Aggiunge codice fiscale, partita iva e codice sdi in footer


## [2.2.26](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.25...2.2.26) - 2021-01-14


#### Code dependencies
| Changes                 | From     | To       | Compare                                                                     |
|-------------------------|----------|----------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.16 | 3.171.17 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.16...3.171.17)       |
| opencontent/ocsensor-ls | 5.0.0    | 5.1.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/5.0.0...5.1.0)    |
| opencontent/ocsensorapi | 5.0.1    | 5.0.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.1...5.0.2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 5.0.0 and 5.1.0](https://github.com/OpencontentCoop/ocsensor/compare/5.0.0...5.1.0)**
* Introduce la possibilità di impostare un geoserver come geocoder
* Espone l’interfaccia di caricamento ajax anche all’operatore

**[opencontent/ocsensorapi changes between 5.0.1 and 5.0.2](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.1...5.0.2)**
* Fix behalf creation in api controller
* Fix hidden-operator-name view data
* Fix hidden-operator-name observers view data


## [2.2.25](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.24...2.2.25) - 2021-01-12


#### Code dependencies
| Changes                 | From     | To       | Compare                                                                     |
|-------------------------|----------|----------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.15 | 3.171.16 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.15...3.171.16)       |
| opencontent/ocsensorapi | 5.0.0    | 5.0.1    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.0...5.0.1) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 5.0.0 and 5.0.1](https://github.com/OpencontentCoop/ocsensorapi/compare/5.0.0...5.0.1)**
* Fix group access in participant service


## [2.2.24](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.23...2.2.24) - 2021-01-12


#### Code dependencies
| Changes                          | From     | To       | Compare                                                                              |
|----------------------------------|----------|----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.12 | 3.171.15 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.12...3.171.15)                |
| opencontent/ocembed-ls           | 1.4.1    | 1.4.2    | [...](https://github.com/OpencontentCoop/ocembed/compare/1.4.1...1.4.2)              |
| opencontent/ocopendata_forms-ls  | 1.6.8    | 1.6.9    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.8...1.6.9)     |
| opencontent/ocsensor-ls          | cac54ad  | 5.0.0    | [...](https://github.com/OpencontentCoop/ocsensor/compare/cac54ad...5.0.0)           |
| opencontent/ocsensorapi          | de2dbc2  | 5.0.0    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/de2dbc2...5.0.0)        |
| symfony/polyfill-ctype           | 7130f34  | c6c942b  | [...](https://github.com/symfony/polyfill-ctype/compare/7130f34...c6c942b)           |
| symfony/polyfill-intl-idn        | 947d45e  | 0eb8293  | [...](https://github.com/symfony/polyfill-intl-idn/compare/947d45e...0eb8293)        |
| symfony/polyfill-intl-normalizer | 3a79a22  | 6e971c8  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/3a79a22...6e971c8) |
| symfony/polyfill-mbstring        | de14691  | f377a3d  | [...](https://github.com/symfony/polyfill-mbstring/compare/de14691...f377a3d)        |
| symfony/polyfill-php72           | 4a4465f  | cc6e6f9  | [...](https://github.com/symfony/polyfill-php72/compare/4a4465f...cc6e6f9)           |
| symfony/polyfill-php73           | 8c0d39c  | a678b42  | [...](https://github.com/symfony/polyfill-php73/compare/8c0d39c...a678b42)           |
| symfony/polyfill-php80           | 54cc82c  | dc3063b  | [...](https://github.com/symfony/polyfill-php80/compare/54cc82c...dc3063b)           |


Relevant changes by repository:

**[opencontent/ocembed-ls changes between 1.4.1 and 1.4.2](https://github.com/OpencontentCoop/ocembed/compare/1.4.1...1.4.2)**
* Evita la richiesta justcheckurl per workaround a problema youtube

**[opencontent/ocopendata_forms-ls changes between 1.6.8 and 1.6.9](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.8...1.6.9)**
* Impedisce l'invio del form alla pressione del tasto invio solo nell'ambito di alpaca-form

**[opencontent/ocsensorapi changes between de2dbc2 and 5.0.0](https://github.com/OpencontentCoop/ocsensorapi/compare/de2dbc2...5.0.0)**
* hotfix reporter as observer


## [2.2.23](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.22-genova...2.2.23) - 2021-01-05


#### Code dependencies
| Changes                          | From    | To       | Compare                                                                              |
|----------------------------------|---------|----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.171.3 | 3.171.12 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.3...3.171.12)                 |
| opencontent/occodicefiscale-ls   | daac5a8 | 2d040a8  | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/daac5a8...2d040a8)  |
| opencontent/ocopendata-ls        | 2.24.7  | 2.25.0   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.7...2.25.0)         |
| opencontent/ocsensor-ls          | 9dbbdb3 | cac54ad  | [...](https://github.com/OpencontentCoop/ocsensor/compare/9dbbdb3...cac54ad)         |
| opencontent/ocsensorapi          | aa88b99 | de2dbc2  | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/aa88b99...de2dbc2)      |
| opencontent/openpa-ls            | 90cedd4 | c036b88  | [...](https://github.com/OpencontentCoop/openpa/compare/90cedd4...c036b88)           |
| symfony/polyfill-ctype           | fade6de | 7130f34  | [...](https://github.com/symfony/polyfill-ctype/compare/fade6de...7130f34)           |
| symfony/polyfill-intl-idn        | 4c489fd | 947d45e  | [...](https://github.com/symfony/polyfill-intl-idn/compare/4c489fd...947d45e)        |
| symfony/polyfill-intl-normalizer | 69609f9 | 3a79a22  | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/69609f9...3a79a22) |
| symfony/polyfill-mbstring        | 401c9d9 | de14691  | [...](https://github.com/symfony/polyfill-mbstring/compare/401c9d9...de14691)        |
| symfony/polyfill-php80           | 3a11f3d | 54cc82c  | [...](https://github.com/symfony/polyfill-php80/compare/3a11f3d...54cc82c)           |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between daac5a8 and 2d040a8](https://github.com/OpencontentCoop/occodicefiscale/compare/daac5a8...2d040a8)**
* Espone i datatype in ocopendata_forms

**[opencontent/ocopendata-ls changes between 2.24.7 and 2.25.0](https://github.com/OpencontentCoop/ocopendata/compare/2.24.7...2.25.0)**
* Permette di bypassare le policy di creazione/aggiornamento in api content repository

**[opencontent/ocsensor-ls changes between 9dbbdb3 and cac54ad](https://github.com/OpencontentCoop/ocsensor/compare/9dbbdb3...cac54ad)**
* Aggiunge la statistica delle adesioni
* Corregge un errore nel template dei messaggi
* Rende dinamico il form contestuale di creazione utente in caso di segnalazione "per conto di"
* Introduce le icone dedicate ai canali (modalità di segnalazione) Impone l'inserimento di una nota privata prima di impostare la segnalazione come "intervento terminato" Permette la chiusura della segnalazione contestualmente all'inserimento di una risposta ufficiale Corregge alcuni bug di visualizzazione Permette la ricerca per id Indica nella dashboard la presenza di commenti in attesa di moderazione
* Corregge la select del tipo di segnalazione nell'interfaccia di creazione ajax

**[opencontent/ocsensorapi changes between aa88b99 and de2dbc2](https://github.com/OpencontentCoop/ocsensorapi/compare/aa88b99...de2dbc2)**
* Add user stat
* Allow user search to user with behalfOfMode active Fix api pagination parameters
* Add ChannelService
* Fix openapi schema
* Set reporter as observer by default
* Add commentsToModerate method
* Add getParticipantNameListByType method (used in csv exporter)

**[opencontent/openpa-ls changes between 90cedd4 and c036b88](https://github.com/OpencontentCoop/openpa/compare/90cedd4...c036b88)**
* Aggiunge il protocollo (https per default) in aws dfs public handler


## [2.2.22-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.21-genova...2.2.22-genova) - 2020-12-21


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.1 | 3.171.3 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.1...3.171.3)             |
| opencontent/ocsensorapi | d301c78 | aa88b99 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/d301c78...aa88b99) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between d301c78 and aa88b99](https://github.com/OpencontentCoop/ocsensorapi/compare/d301c78...aa88b99)**
* Avoid solr exceptions in multiple owners


## [2.2.21-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.20-genova...2.2.21-genova) - 2020-12-21




## [2.2.20-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.19-genova...2.2.20-genova) - 2020-12-18


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 457cec2 | 9dbbdb3 | [...](https://github.com/OpencontentCoop/ocsensor/compare/457cec2...9dbbdb3)    |
| opencontent/ocsensorapi | 3298301 | d301c78 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/3298301...d301c78) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 457cec2 and 9dbbdb3](https://github.com/OpencontentCoop/ocsensor/compare/457cec2...9dbbdb3)**
* Corregge i percorsi dei file di cache per compatibilità con il cluster in redis

**[opencontent/ocsensorapi changes between 3298301 and d301c78](https://github.com/OpencontentCoop/ocsensorapi/compare/3298301...d301c78)**
* Hotfix treenode cache file path


## [2.2.19-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.18-genova...2.2.19-genova) - 2020-12-18


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.171.0 | 3.171.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.171.0...3.171.1)             |
| opencontent/ocsensorapi | 93627a0 | 3298301 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/93627a0...3298301) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 93627a0 and 3298301](https://github.com/OpencontentCoop/ocsensorapi/compare/93627a0...3298301)**
* Hotfix treenode cache file path


## [2.2.18-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.17-genova...2.2.18-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 4f22e79 | 457cec2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4f22e79...457cec2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4f22e79 and 457cec2](https://github.com/OpencontentCoop/ocsensor/compare/4f22e79...457cec2)**
* Evita di emettere un log notice in api controller
* Migliora il messaggio di aiuto nel form di inserimento commento per un operatore


## [2.2.17-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.16-genova...2.2.17-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 7a15dc3 | 4f22e79 | [...](https://github.com/OpencontentCoop/ocsensor/compare/7a15dc3...4f22e79)    |
| opencontent/ocsensorapi | 36aab74 | 93627a0 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/36aab74...93627a0) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 7a15dc3 and 4f22e79](https://github.com/OpencontentCoop/ocsensor/compare/7a15dc3...4f22e79)**
* Corregge i percorsi dei file di cache per compatibilità con il cluster in redis

**[opencontent/ocsensorapi changes between 36aab74 and 93627a0](https://github.com/OpencontentCoop/ocsensorapi/compare/36aab74...93627a0)**
* Fix all sensor cache paths (compat with redis cluster handler)
* Avoid xss


## [2.2.16-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.15-debug...2.2.16-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensorapi | 2d276ac | 36aab74 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/2d276ac...36aab74) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 2d276ac and 36aab74](https://github.com/OpencontentCoop/ocsensorapi/compare/2d276ac...36aab74)**
* Fix treenode cache file path


## [2.2.15-debug](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.15-genova...2.2.15-debug) - 2020-12-17
- Output debug in install script



## [2.2.15-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.14-genova...2.2.15-genova) - 2020-12-17


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.170.0 | 3.171.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.170.0...3.171.0)             |
| opencontent/ocsensor-ls | 4176da2 | 7a15dc3 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4176da2...7a15dc3)    |
| opencontent/ocsensorapi | 05a1611 | 2d276ac | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/05a1611...2d276ac) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4176da2 and 7a15dc3](https://github.com/OpencontentCoop/ocsensor/compare/4176da2...7a15dc3)**
* Migliora l'importazione delle mappe da openstreetmap
* Migliora la visualizzazione degli input form di inserimento

**[opencontent/ocsensorapi changes between 05a1611 and 2d276ac](https://github.com/OpencontentCoop/ocsensorapi/compare/05a1611...2d276ac)**
* Refactorize authorfiscalcode stat filter creating dedicated endpoint


## [2.2.14-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.13-genova...2.2.14-genova) - 2020-12-16


#### Code dependencies
| Changes                      | From    | To      | Compare                                                                         |
|------------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.166.0 | 3.170.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.166.0...3.170.0)             |
| opencontent/ocmultibinary-ls | 2.2.5   | 2.3.0   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.5...2.3.0)   |
| opencontent/ocsensor-ls      | dda7c28 | 4176da2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/dda7c28...4176da2)    |
| opencontent/ocsensorapi      | e79fb0e | 05a1611 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/e79fb0e...05a1611) |
| opencontent/openpa-ls        | a84de51 | 90cedd4 | [...](https://github.com/OpencontentCoop/openpa/compare/a84de51...90cedd4)      |
| php-http/message             | 6e2c574 | d23ac28 | [...](https://github.com/php-http/message/compare/6e2c574...d23ac28)            |


Relevant changes by repository:

**[opencontent/ocmultibinary-ls changes between 2.2.5 and 2.3.0](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.5...2.3.0)**
* Allow to clear all files in fromString method

**[opencontent/ocsensor-ls changes between dda7c28 and 4176da2](https://github.com/OpencontentCoop/ocsensor/compare/dda7c28...4176da2)**
* Migliora il motore di ricerca delle segnalazioni e lo rende interrogabile dalla pagine delle statistiche
* Consente di offuscare il nome degli operatori
* Abilita AnnounceKit

**[opencontent/ocsensorapi changes between e79fb0e and 05a1611](https://github.com/OpencontentCoop/ocsensorapi/compare/e79fb0e...05a1611)**
* Fix PerCategory stat
* Add author fiscal code api filter, add count in search result, bugfix in schema builder
* Does not send AddComment notification to observer group
* Hide operator names if needed by HideOperatorNames setting

**[opencontent/openpa-ls changes between a84de51 and 90cedd4](https://github.com/OpencontentCoop/openpa/compare/a84de51...90cedd4)**
* Permette l'inserimento del codice Web Analytics Italia


## [2.2.13-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.12-genova...2.2.13-genova) - 2020-12-03


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | e1cbc92 | dda7c28 | [...](https://github.com/OpencontentCoop/ocsensor/compare/e1cbc92...dda7c28) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between e1cbc92 and dda7c28](https://github.com/OpencontentCoop/ocsensor/compare/e1cbc92...dda7c28)**
* Corregge un bug nella selezione delle aree nella nuova interfaccia di inserimento


## [2.2.12-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.11-genova...2.2.12-genova) - 2020-12-03


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 84afb0f | e1cbc92 | [...](https://github.com/OpencontentCoop/ocsensor/compare/84afb0f...e1cbc92) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 84afb0f and e1cbc92](https://github.com/OpencontentCoop/ocsensor/compare/84afb0f...e1cbc92)**
* Corregge un bug sulla selezione della privacy nella nuova interfaccia di inserimento


## [2.2.11-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.10-genova...2.2.11-genova) - 2020-12-03


#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php             | 3.165.0 | 3.166.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.165.0...3.166.0)               |
| friendsofsymfony/http-cache | 42c96a0 | fa9abf6 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/42c96a0...fa9abf6) |
| opencontent/ocsensor-ls     | 26f737a | 84afb0f | [...](https://github.com/OpencontentCoop/ocsensor/compare/26f737a...84afb0f)      |
| php-http/message            | 39db36d | 6e2c574 | [...](https://github.com/php-http/message/compare/39db36d...6e2c574)              |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 26f737a and 84afb0f](https://github.com/OpencontentCoop/ocsensor/compare/26f737a...84afb0f)**
* Corregge alcuni problemi di visualizzazione della nuova interfaccia di inserimento


## [2.2.10-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.9-genova...2.2.10-genova) - 2020-12-01


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.164.1 | 3.165.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.164.1...3.165.0)          |
| opencontent/ocsensor-ls | 3e928a1 | 26f737a | [...](https://github.com/OpencontentCoop/ocsensor/compare/3e928a1...26f737a) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 3e928a1 and 26f737a](https://github.com/OpencontentCoop/ocsensor/compare/3e928a1...26f737a)**
* Migliora la versione mobile della nuova interfaccia di creazione segnalazione
* Corregge un bug nel reset del form nella nuova interfaccia di creazione segnalazione


## [2.2.9-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.8-genova...2.2.9-genova) - 2020-12-01


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 909a79b | 3e928a1 | [...](https://github.com/OpencontentCoop/ocsensor/compare/909a79b...3e928a1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 909a79b and 3e928a1](https://github.com/OpencontentCoop/ocsensor/compare/909a79b...3e928a1)**
* Resetta il form di inserimento alla pressione del tasto annulla


## [2.2.8-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.7-genova...2.2.8-genova) - 2020-12-01


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.164.0 | 3.164.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.164.0...3.164.1)             |
| opencontent/ocsensor-ls | 794f869 | 909a79b | [...](https://github.com/OpencontentCoop/ocsensor/compare/794f869...909a79b)    |
| opencontent/ocsensorapi | 4052045 | e79fb0e | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4052045...e79fb0e) |
| php-http/discovery      | 1.12.0  | 1.13.0  | [...](https://github.com/php-http/discovery/compare/1.12.0...1.13.0)            |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 794f869 and 909a79b](https://github.com/OpencontentCoop/ocsensor/compare/794f869...909a79b)**
* Permette all'urp di moderare i commenti Migliora la nuova interfaccia di creazione segnalazione

**[opencontent/ocsensorapi changes between 4052045 and e79fb0e](https://github.com/OpencontentCoop/ocsensorapi/compare/4052045...e79fb0e)**
* Add comment moderation feature


## [2.2.7-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.6-genova...2.2.7-genova) - 2020-11-26


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                      |
|-------------------------|---------|---------|------------------------------------------------------------------------------|
| opencontent/ocsensor-ls | fff6e3b | 794f869 | [...](https://github.com/OpencontentCoop/ocsensor/compare/fff6e3b...794f869) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between fff6e3b and 794f869](https://github.com/OpencontentCoop/ocsensor/compare/fff6e3b...794f869)**
* Corregge alcune imprecisioni dell'interfaccia


## [2.2.6-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.5-genova...2.2.6-genova) - 2020-11-26



#### Installer
- add hide_type_choice and show_smart_gui fields in sensor_root
#### Code dependencies
| Changes                   | From    | To      | Compare                                                                         |
|---------------------------|---------|---------|---------------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.163.1 | 3.164.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.163.1...3.164.0)             |
| opencontent/ocopendata-ls | 2.24.6  | 2.24.7  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.6...2.24.7)    |
| opencontent/ocsensor-ls   | 70f673b | fff6e3b | [...](https://github.com/OpencontentCoop/ocsensor/compare/70f673b...fff6e3b)    |
| opencontent/ocsensorapi   | 2b64cfc | 4052045 | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/2b64cfc...4052045) |


Relevant changes by repository:

**[opencontent/ocopendata-ls changes between 2.24.6 and 2.24.7](https://github.com/OpencontentCoop/ocopendata/compare/2.24.6...2.24.7)**
* opendataTools corregge un bug sul metodo i18n

**[opencontent/ocsensor-ls changes between 70f673b and fff6e3b](https://github.com/OpencontentCoop/ocsensor/compare/70f673b...fff6e3b)**
* Espone in frontend il modulo user/unactivated per il controllo delle utenze in registrazione
* Corregge la selezione dei destinatari dei messaggi privati e preseleziona gli incaricati
* Permette di spuntare il flag Proposta di risposta nei messaggi privati
* Corregge il formato di esportazione in csv per evitare un problema con Excel
* L'autore della segnalazione può ora aggiungere e rimuovere immagini
* Introduce una nuova interfaccia di inserimento (feature flagged)

**[opencontent/ocsensorapi changes between 2b64cfc and 4052045](https://github.com/OpencontentCoop/ocsensorapi/compare/2b64cfc...4052045)**
* Set post status as assigned if a group is selected
* Deny empty message
* Add isResponseProposal field in PrivateMessage
* Add addImage and removeImage actions
* Ad meta property to post structs


## [2.2.5-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.4-genova...2.2.5-genova) - 2020-11-19


#### Code dependencies
| Changes                         | From    | To      | Compare                                                                          |
|---------------------------------|---------|---------|----------------------------------------------------------------------------------|
| aws/aws-sdk-php                 | 3.161.0 | 3.163.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.161.0...3.163.1)              |
| opencontent/ocmultibinary-ls    | 2.2.4   | 2.2.5   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.4...2.2.5)    |
| opencontent/ocopendata-ls       | 2.24.4  | 2.24.6  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.4...2.24.6)     |
| opencontent/ocopendata_forms-ls | 1.6.7   | 1.6.8   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.7...1.6.8) |
| opencontent/ocsensor-ls         | 4df2aa4 | 70f673b | [...](https://github.com/OpencontentCoop/ocsensor/compare/4df2aa4...70f673b)     |
| opencontent/ocsensorapi         | 63645af | 2b64cfc | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/63645af...2b64cfc)  |


Relevant changes by repository:

**[opencontent/ocmultibinary-ls changes between 2.2.4 and 2.2.5](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.4...2.2.5)**
* Add ini setting to configure allowed file extension per attribute

**[opencontent/ocopendata-ls changes between 2.24.4 and 2.24.6](https://github.com/OpencontentCoop/ocopendata/compare/2.24.4...2.24.6)**
* Corregge un bug che si verificava nella pubblicazione di contenuti in installazioni multilingua
* Mantiene la stessa dimensione dell'array di risposta nelle richieste select-fields anche se manca la traduzione nella lingua corrente
* opendataTools i18n visualizza la prima traduzione esistente qualora non esistesse nella lingua corrente né nella lingua di fallback

**[opencontent/ocopendata_forms-ls changes between 1.6.7 and 1.6.8](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.7...1.6.8)**
* Permette di tradurre di un contenuto qualora si tenti di modificarlo in una lingua non ancora presente

**[opencontent/ocsensor-ls changes between 4df2aa4 and 70f673b](https://github.com/OpencontentCoop/ocsensor/compare/4df2aa4...70f673b)**
* Migliora il messaggio di errore nell'accesso a una segnalazione privata
* Migliora la funzionalità dei messaggi privati, dando la possibilità di specificare i destinatari
* Specifica i tipi di file ammessi nel campo senso_post/images

**[opencontent/ocsensorapi changes between 63645af and 2b64cfc](https://github.com/OpencontentCoop/ocsensorapi/compare/63645af...2b64cfc)**
* Send notification on private message only to specified receivers


## [2.2.4-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.3-genova...2.2.4-genova) - 2020-11-13


#### Code dependencies
| Changes                      | From    | To      | Compare                                                                       |
|------------------------------|---------|---------|-------------------------------------------------------------------------------|
| aws/aws-sdk-php              | 3.159.1 | 3.161.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.159.1...3.161.0)           |
| opencontent/ocmultibinary-ls | 2.2.3   | 2.2.4   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.3...2.2.4) |
| php-http/guzzle6-adapter     | bd7b405 | 6f108cf | [...](https://github.com/php-http/guzzle6-adapter/compare/bd7b405...6f108cf)  |


Relevant changes by repository:

**[opencontent/ocmultibinary-ls changes between 2.2.3 and 2.2.4](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.3...2.2.4)**
* Add error translations


## [2.2.3-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.2-genova...2.2.3-genova) - 2020-11-11
- Change base image with php7.2.33-clean
- Fix category access in sensor anonymous role

#### Code dependencies
| Changes                          | From     | To      | Compare                                                                              |
|----------------------------------|----------|---------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.158.13 | 3.159.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.158.13...3.159.1)                 |
| composer/installers              | f69761f  | 207a91f | [...](https://github.com/composer/installers/compare/f69761f...207a91f)              |
| opencontent/ocinstaller          | 35c8665  | 7a4ec2b | [...](https://github.com/OpencontentCoop/ocinstaller/compare/35c8665...7a4ec2b)      |
| opencontent/ocmultibinary-ls     | 2.2.2    | 2.2.3   | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.2...2.2.3)        |
| opencontent/ocsensor-ls          | b836850  | 4df2aa4 | [...](https://github.com/OpencontentCoop/ocsensor/compare/b836850...4df2aa4)         |
| opencontent/openpa-ls            | fd363e3  | a84de51 | [...](https://github.com/OpencontentCoop/openpa/compare/fd363e3...a84de51)           |
| php-http/message                 | 1.9.1    | 39db36d | [...](https://github.com/php-http/message/compare/1.9.1...39db36d)                   |
| symfony/polyfill-ctype           | f4ba089  | fade6de | [...](https://github.com/symfony/polyfill-ctype/compare/f4ba089...fade6de)           |
| symfony/polyfill-intl-idn        | 3b75acd  | 4c489fd | [...](https://github.com/symfony/polyfill-intl-idn/compare/3b75acd...4c489fd)        |
| symfony/polyfill-intl-normalizer | 727d109  | 69609f9 | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/727d109...69609f9) |
| symfony/polyfill-mbstring        | 39d483b  | 401c9d9 | [...](https://github.com/symfony/polyfill-mbstring/compare/39d483b...401c9d9)        |
| symfony/polyfill-php72           | cede45f  | 4a4465f | [...](https://github.com/symfony/polyfill-php72/compare/cede45f...4a4465f)           |
| symfony/polyfill-php73           | 8ff431c  | 8c0d39c | [...](https://github.com/symfony/polyfill-php73/compare/8ff431c...8c0d39c)           |
| symfony/polyfill-php80           | e70aa8b  | 3a11f3d | [...](https://github.com/symfony/polyfill-php80/compare/e70aa8b...3a11f3d)           |
| zetacomponents/console-tools     | 1.7.1    | 1.7.2   | [...](https://github.com/zetacomponents/ConsoleTools/compare/1.7.1...1.7.2)          |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 35c8665 and 7a4ec2b](https://github.com/OpencontentCoop/ocinstaller/compare/35c8665...7a4ec2b)**
* Fix dump_tag_tree tool, add recaptcha3 installer

**[opencontent/ocmultibinary-ls changes between 2.2.2 and 2.2.3](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.2...2.2.3)**
* Add missing translation

**[opencontent/ocsensor-ls changes between b836850 and 4df2aa4](https://github.com/OpencontentCoop/ocsensor/compare/b836850...4df2aa4)**
* Migliora la visualizzazione del menu di selezione delle zone se disabilitato

**[opencontent/openpa-ls changes between fd363e3 and a84de51](https://github.com/OpencontentCoop/openpa/compare/fd363e3...a84de51)**
* Permette la configurazione di Google Recaptcha v3
* Aggiorna le configurazioni degli attributi di classe direttamente dal openpa/recaptcha


## [2.2.2-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.1-genova...2.2.2-genova) - 2020-10-24


#### Code dependencies
| Changes                 | From    | To      | Compare                                                                         |
|-------------------------|---------|---------|---------------------------------------------------------------------------------|
| opencontent/ocsensorapi | 9f636eb | 63645af | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/9f636eb...63645af) |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 9f636eb and 63645af](https://github.com/OpencontentCoop/ocsensorapi/compare/9f636eb...63645af)**
* Fix  ocmultibinary download url


## [2.2.1-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.2.0-genova...2.2.1-genova) - 2020-10-24


#### Code dependencies
| Changes                          | From      | To      | Compare                                                                              |
|----------------------------------|-----------|---------|--------------------------------------------------------------------------------------|
| opencontent/ocsensor-ls          | 6e8d7ee   | b836850 | [...](https://github.com/OpencontentCoop/ocsensor/compare/6e8d7ee...b836850)         |
| paragonie/random_compat          | v9.99.100 |         |                                                                                      |
| symfony/polyfill-ctype           | aed5969   | f4ba089 | [...](https://github.com/symfony/polyfill-ctype/compare/aed5969...f4ba089)           |
| symfony/polyfill-intl-idn        | fd17ae0   | 3b75acd | [...](https://github.com/symfony/polyfill-intl-idn/compare/fd17ae0...3b75acd)        |
| symfony/polyfill-intl-normalizer | 8db0ae7   | 727d109 | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/8db0ae7...727d109) |
| symfony/polyfill-mbstring        | b5f7b93   | 39d483b | [...](https://github.com/symfony/polyfill-mbstring/compare/b5f7b93...39d483b)        |
| symfony/polyfill-php70           | 3fe4140   |         |                                                                                      |
| symfony/polyfill-php72           | beecef6   | cede45f | [...](https://github.com/symfony/polyfill-php72/compare/beecef6...cede45f)           |
| symfony/polyfill-php73           | 9d920e3   | 8ff431c | [...](https://github.com/symfony/polyfill-php73/compare/9d920e3...8ff431c)           |
| symfony/polyfill-php80           | f54ef00   | e70aa8b | [...](https://github.com/symfony/polyfill-php80/compare/f54ef00...e70aa8b)           |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 6e8d7ee and b836850](https://github.com/OpencontentCoop/ocsensor/compare/6e8d7ee...b836850)**
* Evita di esporre il codice fiscale nel csv export
* Visualizza gli utenti disabilitati in sensor/settings


## [2.2.0-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.1.0-genova...2.2.0-genova) - 2020-10-24


#### Code dependencies
| Changes                          | From           | To        | Compare                                                                              |
|----------------------------------|----------------|-----------|--------------------------------------------------------------------------------------|
| aws/aws-sdk-php                  | 3.158.6        | 3.158.13  | [...](https://github.com/aws/aws-sdk-php/compare/3.158.6...3.158.13)                 |
| composer/installers              | c462a69        | f69761f   | [...](https://github.com/composer/installers/compare/c462a69...f69761f)              |
| guzzlehttp/promises              | 60d379c        | ddfeedf   | [...](https://github.com/guzzle/promises/compare/60d379c...ddfeedf)                  |
| opencontent/ocinstaller          | f5c1cc8        | 35c8665   | [...](https://github.com/OpencontentCoop/ocinstaller/compare/f5c1cc8...35c8665)      |
| opencontent/ocsensor-ls          | b31cc90        | 6e8d7ee   | [...](https://github.com/OpencontentCoop/ocsensor/compare/b31cc90...6e8d7ee)         |
| opencontent/ocsensorapi          | e890c7f        | 9f636eb   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/e890c7f...9f636eb)      |
| opencontent/openpa_sensor-ls     | 4.2.3          | 4.2.4     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.3...4.2.4)        |
| paragonie/random_compat          | v9.99.99.x-dev | v9.99.100 | [...](https://github.com/paragonie/random_compat/compare/v9.99.99.x-dev...v9.99.100) |
| symfony/polyfill-ctype           | 1c30264        | aed5969   | [...](https://github.com/symfony/polyfill-ctype/compare/1c30264...aed5969)           |
| symfony/polyfill-intl-idn        | 045643b        | fd17ae0   | [...](https://github.com/symfony/polyfill-intl-idn/compare/045643b...fd17ae0)        |
| symfony/polyfill-intl-normalizer | 37078a8        | 8db0ae7   | [...](https://github.com/symfony/polyfill-intl-normalizer/compare/37078a8...8db0ae7) |
| symfony/polyfill-mbstring        | 48928d4        | b5f7b93   | [...](https://github.com/symfony/polyfill-mbstring/compare/48928d4...b5f7b93)        |
| symfony/polyfill-php70           | 0dd93f2        | 3fe4140   | [...](https://github.com/symfony/polyfill-php70/compare/0dd93f2...3fe4140)           |
| symfony/polyfill-php72           | dc6ef20        | beecef6   | [...](https://github.com/symfony/polyfill-php72/compare/dc6ef20...beecef6)           |
| symfony/polyfill-php73           | fffa1a5        | 9d920e3   | [...](https://github.com/symfony/polyfill-php73/compare/fffa1a5...9d920e3)           |
| symfony/polyfill-php80           | c3616e7        | f54ef00   | [...](https://github.com/symfony/polyfill-php80/compare/c3616e7...f54ef00)           |


Relevant changes by repository:

**[opencontent/ocinstaller changes between f5c1cc8 and 35c8665](https://github.com/OpencontentCoop/ocinstaller/compare/f5c1cc8...35c8665)**
* Make bigint type to ezcontentobject.published and ezcontentobject.modified
* add install pgcrypto script

**[opencontent/ocsensor-ls changes between b31cc90 and 6e8d7ee](https://github.com/OpencontentCoop/ocsensor/compare/b31cc90...6e8d7ee)**
* Disattiva di default la selezione dei destinatari nelle note private Corregge un bug nel comportamento della select dei gruppi/operatori Espone le configurazioni dei commenti e dei destinatari nelle note private in sensor/config
* Corregge i valori della tendina Tipo nella ricerca segnalazioni in base alla definizione della classe sensor_post

**[opencontent/ocsensorapi changes between e890c7f and 9f636eb](https://github.com/OpencontentCoop/ocsensorapi/compare/e890c7f...9f636eb)**
* Add CanSelectReceiverInPrivateMessage permission and use UseDirectPrivateMessage settings
* Add PostTypeService, fix some warnings

**[opencontent/openpa_sensor-ls changes between 4.2.3 and 4.2.4](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.3...4.2.4)**
* Reindicizza in background gli operatori quando viene modificato un gruppo


## [2.1.0-genova](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.8...2.1.0-genova) - 2020-10-19


#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.154.5 | 3.158.6 | [...](https://github.com/aws/aws-sdk-php/compare/3.154.5...3.158.6)                    |
| clue/stream-filter                 | v1.4.1  | v1.5.0  | [...](https://github.com/clue/php-stream-filter/compare/v1.4.1...v1.5.0)               |
| guzzlehttp/promises                | bbf3b20 | 60d379c | [...](https://github.com/guzzle/promises/compare/bbf3b20...60d379c)                    |
| league/event                       | 7823f10 | 2.2.0   | [...](https://github.com/thephpleague/event/compare/7823f10...2.2.0)                   |
| opencontent/occodicefiscale-ls     | 542aa99 | daac5a8 | [...](https://github.com/OpencontentCoop/occodicefiscale/compare/542aa99...daac5a8)    |
| opencontent/ocinstaller            | 8a20be4 | f5c1cc8 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/8a20be4...f5c1cc8)        |
| opencontent/ocopendata-ls          | 2.24.3  | 2.24.4  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.3...2.24.4)           |
| opencontent/ocopendata_forms-ls    | 1.6.4   | 1.6.7   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.4...1.6.7)       |
| opencontent/ocsensor-ls            | 4.8.1   | b31cc90 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.8.1...b31cc90)             |
| opencontent/ocsensorapi            | 4.8.0   | e890c7f | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.8.0...e890c7f)          |
| opencontent/openpa-ls              | 9f93297 | fd363e3 | [...](https://github.com/OpencontentCoop/openpa/compare/9f93297...fd363e3)             |
| opencontent/openpa_sensor-ls       | 4.2.2   | 4.2.3   | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.2...4.2.3)          |
| php-http/discovery                 | 1.10.0  | 1.12.0  | [...](https://github.com/php-http/discovery/compare/1.10.0...1.12.0)                   |
| php-http/message                   | 1.9.0   | 1.9.1   | [...](https://github.com/php-http/message/compare/1.9.0...1.9.1)                       |
| psr/container                      | b1fbdff | 381524e | [...](https://github.com/php-fig/container/compare/b1fbdff...381524e)                  |
| psr/http-client                    | 2dfb5f6 | 22b2ef5 | [...](https://github.com/php-fig/http-client/compare/2dfb5f6...22b2ef5)                |
| symfony/deprecation-contracts      | 5fa56b4 | d940483 | [...](https://github.com/symfony/deprecation-contracts/compare/5fa56b4...d940483)      |
| symfony/event-dispatcher           | 98e8d61 | 5.x-dev | [...](https://github.com/symfony/event-dispatcher/compare/98e8d61...5.x-dev)           |
| symfony/event-dispatcher-contracts | 0ba7d54 | 5e8ae4d | [...](https://github.com/symfony/event-dispatcher-contracts/compare/0ba7d54...5e8ae4d) |
| symfony/options-resolver           | c2565c6 | 5.x-dev | [...](https://github.com/symfony/options-resolver/compare/c2565c6...5.x-dev)           |
| symfony/polyfill-php72             | 639447d | dc6ef20 | [...](https://github.com/symfony/polyfill-php72/compare/639447d...dc6ef20)             |


Relevant changes by repository:

**[opencontent/occodicefiscale-ls changes between 542aa99 and daac5a8](https://github.com/OpencontentCoop/occodicefiscale/compare/542aa99...daac5a8)**
* Considera solo gli oggetti pubblicati nel calcolo dei doppioni

**[opencontent/ocinstaller changes between 8a20be4 and f5c1cc8](https://github.com/OpencontentCoop/ocinstaller/compare/8a20be4...f5c1cc8)**
* Add remove extra locations feature in content tree installer
* Now inside a resource can import another resource

**[opencontent/ocopendata-ls changes between 2.24.3 and 2.24.4](https://github.com/OpencontentCoop/ocopendata/compare/2.24.3...2.24.4)**
* Aumento il limite massimo di ricerca in env default a 300 (per compatibilità con ocopendata_forms)

**[opencontent/ocopendata_forms-ls changes between 1.6.4 and 1.6.7](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.4...1.6.7)**
* Permette l'utilizzo della libreria UploadFieldConnector in esecuzioni da linea di comando
* Rimuove un log di debugging
* Corregge un problema di configurazione

**[opencontent/ocsensor-ls changes between 4.8.1 and b31cc90](https://github.com/OpencontentCoop/ocsensor/compare/4.8.1...b31cc90)**
* Abilita l'approver a modificare il riferimento per il cittadino
* Abilita l'approver a modificare il riferimento per il cittadino
* Introduce la gestione dei gruppi di incaricati
* Corregge la selezione degli operatori nell'interfaccia di modifica incaricato
* Permette di filtrare l'esecuzione dei webhook per gruppo
* Considera le sottocategorie e le sottoaree nei filtri di categoria e di area del motore di ricerca della pagina Segnalazioni
* Corregge un bug per cui non venivano correttamente visualizzati i filtri dei webhook

**[opencontent/ocsensorapi changes between 4.8.0 and e890c7f](https://github.com/OpencontentCoop/ocsensorapi/compare/4.8.0...e890c7f)**
* Introduce la gestione dei gruppi di incaricati
* Corregge la versione della dipendenza league/event
* Fix assign action logic
* Add notification target configurable per notification type
* Fix log messages and filename
* Fix code comment
* Add addCurrentUserAddress flag
* Set all mail receivers as primary receiver

**[opencontent/openpa-ls changes between 9f93297 and fd363e3](https://github.com/OpencontentCoop/openpa/compare/9f93297...fd363e3)**
* Corregge l'id del tree menu in caso di menu virtualizzato su alberatura di tag

**[opencontent/openpa_sensor-ls changes between 4.2.2 and 4.2.3](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.2...4.2.3)**
* Svuota la cache degli operatori e dei gruppi alla modifica


## [2.0.8](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.7...2.0.8) - 2020-09-18


#### Code dependencies
| Changes                            | From    | To      | Compare                                                                             |
|------------------------------------|---------|---------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.152.0 | 3.154.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.152.0...3.154.5)                 |
| opencontent/ezpostgresqlcluster-ls | 1c50dd6 | f37c194 | [...](https://github.com/Opencontent/ezpostgresqlcluster/compare/1c50dd6...f37c194) |
| opencontent/ocopendata-ls          | 2.24.2  | 2.24.3  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.2...2.24.3)        |
| opencontent/ocsensor-ls            | 4.8.0   | 4.8.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.8.0...4.8.1)            |
| opencontent/ocsensorapi            | 4.7.0   | 4.8.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.7.0...4.8.0)         |
| psr/container                      | fc1bc36 | b1fbdff | [...](https://github.com/php-fig/container/compare/fc1bc36...b1fbdff)               |
| psr/event-dispatcher               | 3b1c107 | 3ef040d | [...](https://github.com/php-fig/event-dispatcher/compare/3b1c107...3ef040d)        |
| psr/http-factory                   | 1a2099a | 36fa03d | [...](https://github.com/php-fig/http-factory/compare/1a2099a...36fa03d)            |
| psr/log                            | 0f73288 | dd738d0 | [...](https://github.com/php-fig/log/compare/0f73288...dd738d0)                     |
| symfony/polyfill-mbstring          | a6977d6 | 48928d4 | [...](https://github.com/symfony/polyfill-mbstring/compare/a6977d6...48928d4)       |
| symfony/polyfill-php80             | d87d576 | c3616e7 | [...](https://github.com/symfony/polyfill-php80/compare/d87d576...c3616e7)          |


Relevant changes by repository:

**[opencontent/ezpostgresqlcluster-ls changes between 1c50dd6 and f37c194](https://github.com/Opencontent/ezpostgresqlcluster/compare/1c50dd6...f37c194)**
* Check OpenPABase class exists

**[opencontent/ocopendata-ls changes between 2.24.2 and 2.24.3](https://github.com/OpencontentCoop/ocopendata/compare/2.24.2...2.24.3)**
* Corregge un bug sul parsing dei tag nel AttributeConverter

**[opencontent/ocsensor-ls changes between 4.8.0 and 4.8.1](https://github.com/OpencontentCoop/ocsensor/compare/4.8.0...4.8.1)**
* Aggiunge uno script per configurare il password lifetime

**[opencontent/ocsensorapi changes between 4.7.0 and 4.8.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.7.0...4.8.0)**
* Add super user capabilities


## [2.0.7](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.6...2.0.7) - 2020-09-07


#### Code dependencies
| Changes                            | From     | To      | Compare                                                                                |
|------------------------------------|----------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.147.12 | 3.152.0 | [...](https://github.com/aws/aws-sdk-php/compare/3.147.12...3.152.0)                   |
| mtdowling/jmespath.php             | 42dae2c  | 30dfa00 | [...](https://github.com/jmespath/jmespath.php/compare/42dae2c...30dfa00)              |
| opencontent/ocbootstrap-ls         | 1.10.1   | 1.10.2  | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.1...1.10.2)          |
| opencontent/ocopendata_forms-ls    | 1.6.3    | 1.6.4   | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.3...1.6.4)       |
| opencontent/ocsensor-ls            | 4.7.0    | 4.8.0   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.7.0...4.8.0)               |
| opencontent/ocsensorapi            | 4.6.0    | 4.7.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.6.0...4.7.0)            |
| php-http/discovery                 | 64a18cc  | 1.10.0  | [...](https://github.com/php-http/discovery/compare/64a18cc...1.10.0)                  |
| php-http/message                   | 226ba9f  | 1.9.0   | [...](https://github.com/php-http/message/compare/226ba9f...1.9.0)                     |
| symfony/deprecation-contracts      | 5e20b83  | 5fa56b4 | [...](https://github.com/symfony/deprecation-contracts/compare/5e20b83...5fa56b4)      |
| symfony/event-dispatcher           | bbb461a  | 98e8d61 | [...](https://github.com/symfony/event-dispatcher/compare/bbb461a...98e8d61)           |
| symfony/event-dispatcher-contracts | f6f613d  | 0ba7d54 | [...](https://github.com/symfony/event-dispatcher-contracts/compare/f6f613d...0ba7d54) |
| symfony/options-resolver           | e25bc4b  | c2565c6 | [...](https://github.com/symfony/options-resolver/compare/e25bc4b...c2565c6)           |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.10.1 and 1.10.2](https://github.com/OpencontentCoop/ocbootstrap/compare/1.10.1...1.10.2)**
* Corregge un bug che impediva la visualizzazione dei testi di aiuto nei form dinamici

**[opencontent/ocopendata_forms-ls changes between 1.6.3 and 1.6.4](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.3...1.6.4)**
* Espone l'indirizzo da geocoder nominatim in modo meno verboso

**[opencontent/ocsensor-ls changes between 4.7.0 and 4.8.0](https://github.com/OpencontentCoop/ocsensor/compare/4.7.0...4.8.0)**
* Permette di rimuovere un osservatore da interfaccia

**[opencontent/ocsensorapi changes between 4.6.0 and 4.7.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.6.0...4.7.0)**
* Add remove observer action


## [2.0.6](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.5...2.0.6) - 2020-08-05
- Update publiccode
- Fix Admin role, sensor operator group select

#### Code dependencies
| Changes                              | From    | To             | Compare                                                                                |
|--------------------------------------|---------|----------------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                      | 3.143.1 | 3.147.12       | [...](https://github.com/aws/aws-sdk-php/compare/3.143.1...3.147.12)                   |
| friendsofsymfony/http-cache          | e6e9218 | 42c96a0        | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/e6e9218...42c96a0)      |
| mtdowling/jmespath.php               | 52168cb | 42dae2c        | [...](https://github.com/jmespath/jmespath.php/compare/52168cb...42dae2c)              |
| opencontent/ocbootstrap-ls           | 1.9.8   | 1.10.1         | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.8...1.10.1)           |
| opencontent/ocinstaller              | 5a366f4 | 8a20be4        | [...](https://github.com/OpencontentCoop/ocinstaller/compare/5a366f4...8a20be4)        |
| opencontent/ocoperatorscollection-ls | 2.1.1   | 2.2.0          | [...](https://github.com/OpencontentCoop/ocoperatorscollection/compare/2.1.1...2.2.0)  |
| opencontent/ocsensor-ls              | 4.5.5   | 4.7.0          | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.5.5...4.7.0)               |
| opencontent/ocsensorapi              | 4.5.3   | 4.6.0          | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.3...4.6.0)            |
| opencontent/openpa-ls                | c9105eb | 9f93297        | [...](https://github.com/OpencontentCoop/openpa/compare/c9105eb...9f93297)             |
| opencontent/openpa_sensor-ls         | 4.2.1   | 4.2.2          | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.1...4.2.2)          |
| opencontent/openpa_theme_2014-ls     | 2.15.0  | 2.15.2         | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.0...2.15.2)    |
| paragonie/random_compat              |         | v9.99.99.x-dev |                                                                                        |
| php-http/client-common               | 7c27fe6 | e37e46c        | [...](https://github.com/php-http/client-common/compare/7c27fe6...e37e46c)             |
| php-http/discovery                   | 1.8.0   | 64a18cc        | [...](https://github.com/php-http/discovery/compare/1.8.0...64a18cc)                   |
| php-http/httplug                     | c9b2424 | 191a0a1        | [...](https://github.com/php-http/httplug/compare/c9b2424...191a0a1)                   |
| php-http/promise                     | 02ee67f | 4c4c1f9        | [...](https://github.com/php-http/promise/compare/02ee67f...4c4c1f9)                   |
| psr/http-client                      | fd5d37a | 2dfb5f6        | [...](https://github.com/php-fig/http-client/compare/fd5d37a...2dfb5f6)                |
| psr/http-factory                     |         | 1a2099a        |                                                                                        |
| symfony/event-dispatcher             | 0403953 | bbb461a        | [...](https://github.com/symfony/event-dispatcher/compare/0403953...bbb461a)           |
| symfony/event-dispatcher-contracts   | 3bf9307 | f6f613d        | [...](https://github.com/symfony/event-dispatcher-contracts/compare/3bf9307...f6f613d) |
| symfony/options-resolver             | 9ae2b94 | e25bc4b        | [...](https://github.com/symfony/options-resolver/compare/9ae2b94...e25bc4b)           |
| symfony/polyfill-ctype               | 2edd75b | 1c30264        | [...](https://github.com/symfony/polyfill-ctype/compare/2edd75b...1c30264)             |
| symfony/polyfill-intl-idn            | a57f816 | 045643b        | [...](https://github.com/symfony/polyfill-intl-idn/compare/a57f816...045643b)          |
| symfony/polyfill-intl-normalizer     |         | 37078a8        |                                                                                        |
| symfony/polyfill-mbstring            | 7110338 | a6977d6        | [...](https://github.com/symfony/polyfill-mbstring/compare/7110338...a6977d6)          |
| symfony/polyfill-php70               |         | 0dd93f2        |                                                                                        |
| symfony/polyfill-php72               | 3d9c70f | 639447d        | [...](https://github.com/symfony/polyfill-php72/compare/3d9c70f...639447d)             |
| symfony/polyfill-php73               | fa0837f | fffa1a5        | [...](https://github.com/symfony/polyfill-php73/compare/fa0837f...fffa1a5)             |
| symfony/polyfill-php80               | 4a5b6bb | d87d576        | [...](https://github.com/symfony/polyfill-php80/compare/4a5b6bb...d87d576)             |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.8 and 1.10.1](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.8...1.10.1)**
* Corregge un bug che impediva la corretta visualizzazione dei checkbox nei form dinamici
* Corregge un errore nella pagina di conferma pubblicazione
* Introduce i template bootstrap 4 per ezsurvey
* Corregge la visualizzazione dei form per ezsurvey in bootstrap 4

**[opencontent/ocinstaller changes between 5a366f4 and 8a20be4](https://github.com/OpencontentCoop/ocinstaller/compare/5a366f4...8a20be4)**
* Write installer log to file
* Fix workflow installer
* Fix expiryPassword in dryrun
* Add change_state, change_section, tag_description Bugifx
* Add reindex step

**[opencontent/ocoperatorscollection-ls changes between 2.1.1 and 2.2.0](https://github.com/OpencontentCoop/ocoperatorscollection/compare/2.1.1...2.2.0)**
* Avoid switch to user with admin capabilities

**[opencontent/ocsensor-ls changes between 4.5.5 and 4.7.0](https://github.com/OpencontentCoop/ocsensor/compare/4.5.5...4.7.0)**
* Aggiunge a sensor la notifica per l'evento add_approver e corregge alcuni bug di implementazione dei template di notifica
* Evita di rimuovere l'operatore spostandolo nel gruppo utenti
* Inietta nelle definizioni di CanClose e CanResponde gli utenti urp nel caso in cui sia attiva la configurazione ForceUrpApproverOnFix Imposta la configurazione ForceUrpApproverOnFix attiva per default

**[opencontent/ocsensorapi changes between 4.5.3 and 4.6.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.3...4.6.0)**
* Add on_add_approver notification and fix the participant mail address finder
* Set restrict responders in CanClose and CanRespond permission definitions

**[opencontent/openpa-ls changes between c9105eb and 9f93297](https://github.com/OpencontentCoop/openpa/compare/c9105eb...9f93297)**
* Corregge un bug in openpa/object
* Corregge la fetch per il controllo degli stati
* Permette a content_link di riconoscere se un link è al nodo o a un nodo/link esterno
* Corregge un possibile errore nel calcolo delle aree tematiche
* Corregge un typo in cookie
* Migliora la visualizzazione della versione del software

**[opencontent/openpa_sensor-ls changes between 4.2.1 and 4.2.2](https://github.com/OpencontentCoop/openpa_sensor/compare/4.2.1...4.2.2)**
* Corregge un bug sul calcolo dell'url degli asset

**[opencontent/openpa_theme_2014-ls changes between 2.15.0 and 2.15.2](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.15.0...2.15.2)**
* Rimuove lo spellcheck di solr
* Corregge il link al download csv in trasparenza


## [2.0.5](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.4...2.0.5) - 2020-06-26


#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.138.8 | 3.143.1 | [...](https://github.com/aws/aws-sdk-php/compare/3.138.8...3.143.1)                    |
| composer/installers                | 8669edf | c462a69 | [...](https://github.com/composer/installers/compare/8669edf...c462a69)                |
| guzzlehttp/promises                | 89b1a76 | bbf3b20 | [...](https://github.com/guzzle/promises/compare/89b1a76...bbf3b20)                    |
| justinrainbow/json-schema          | 5.2.9   | 5.x-dev | [...](https://github.com/justinrainbow/json-schema/compare/5.2.9...5.x-dev)            |
| opencontent/ocinstaller            | 7ef45b7 | 5a366f4 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/7ef45b7...5a366f4)        |
| opencontent/ocsensor-ls            | 4.5.4   | 4.5.5   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.5.4...4.5.5)               |
| opencontent/ocsensorapi            | 4.5.2   | 4.5.3   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.2...4.5.3)            |
| opencontent/openpa-ls              | 5dc1290 | c9105eb | [...](https://github.com/OpencontentCoop/openpa/compare/5dc1290...c9105eb)             |
| opencontent/openpa_userprofile-ls  | 2e192b1 | 4c7a67e | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/2e192b1...4c7a67e) |
| php-http/discovery                 | 7e30d5c | 1.8.0   | [...](https://github.com/php-http/discovery/compare/7e30d5c...1.8.0)                   |
| symfony/deprecation-contracts      | ede224d | 5e20b83 | [...](https://github.com/symfony/deprecation-contracts/compare/ede224d...5e20b83)      |
| symfony/event-dispatcher           | 8a53fd3 | 0403953 | [...](https://github.com/symfony/event-dispatcher/compare/8a53fd3...0403953)           |
| symfony/event-dispatcher-contracts | 405952c | 3bf9307 | [...](https://github.com/symfony/event-dispatcher-contracts/compare/405952c...3bf9307) |
| symfony/options-resolver           | f3f9b9c | 9ae2b94 | [...](https://github.com/symfony/options-resolver/compare/f3f9b9c...9ae2b94)           |
| symfony/polyfill-ctype             | e94c8b1 | 2edd75b | [...](https://github.com/symfony/polyfill-ctype/compare/e94c8b1...2edd75b)             |
| symfony/polyfill-intl-idn          | v1.17.0 | a57f816 | [...](https://github.com/symfony/polyfill-intl-idn/compare/v1.17.0...a57f816)          |
| symfony/polyfill-mbstring          | fa79b11 | 7110338 | [...](https://github.com/symfony/polyfill-mbstring/compare/fa79b11...7110338)          |
| symfony/polyfill-php72             | f048e61 | 3d9c70f | [...](https://github.com/symfony/polyfill-php72/compare/f048e61...3d9c70f)             |
| symfony/polyfill-php73             | a760d89 | fa0837f | [...](https://github.com/symfony/polyfill-php73/compare/a760d89...fa0837f)             |
| symfony/polyfill-php80             | 5e30b27 | 4a5b6bb | [...](https://github.com/symfony/polyfill-php80/compare/5e30b27...4a5b6bb)             |
| zetacomponents/mail                | 1.9.1   | 1.9.2   | [...](https://github.com/zetacomponents/Mail/compare/1.9.1...1.9.2)                    |


Relevant changes by repository:

**[opencontent/ocinstaller changes between 7ef45b7 and 5a366f4](https://github.com/OpencontentCoop/ocinstaller/compare/7ef45b7...5a366f4)**
* Add classid var function calling eZContentClass::classIDByIdentifier
* add classattributeid and classattributeid_list expression function
* Check trash Allow extra root datadir path
* Add --force option to ignore version check Add add_tag, remove_tag, move_tag to handle single tag modification
* Add patch_content to update single content attribute
* Fix compatibility with eZ version 5.90.0
* Add sort data in patch_content Fix patch_content error handling Improve version_compare Fix bug in tagtree synonyms Add dump script tools

**[opencontent/ocsensor-ls changes between 4.5.4 and 4.5.5](https://github.com/OpencontentCoop/ocsensor/compare/4.5.4...4.5.5)**
* Assegna gli osservatori configurati nelle zone al primo accesso al post del riferimento per il cittadino

**[opencontent/ocsensorapi changes between 4.5.2 and 4.5.3](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.2...4.5.3)**
* Fire on_approver_first_read custom event and add ApproverFirstReadListener

**[opencontent/openpa-ls changes between 5dc1290 and c9105eb](https://github.com/OpencontentCoop/openpa/compare/5dc1290...c9105eb)**
* Corregge il riconoscimento automatico del software in uso per la visualizzazione delle metriche di utilizzo
* Corregge un bug nella definizione degli handler dei blocchi
* Permette la personalizzazione dei valori head/meta da openpa/seo
* Visualizza la versione dell'installer se presente in CreditSettings/CodeVersion

**[opencontent/openpa_userprofile-ls changes between 2e192b1 and 4c7a67e](https://github.com/OpencontentCoop/openpa_userprofile/compare/2e192b1...4c7a67e)**
* Considera gli attributi raccomandati per la validazione del profilo


## [2.0.4](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.3...2.0.4) - 2020-06-10
- Fix gitlab ci branch/tag name



## [2.0.3](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.2...2.0.3) - 2020-06-10
- Make solr data dir in Dockerfile.solr



## [2.0.2](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.1...2.0.2) - 2020-06-08
- Added publiccode
- Update deps

#### Code dependencies
| Changes                            | From    | To      | Compare                                                                                |
|------------------------------------|---------|---------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.137.5 | 3.138.8 | [...](https://github.com/aws/aws-sdk-php/compare/3.137.5...3.138.8)                    |
| friendsofsymfony/http-cache        | f0cd186 | e6e9218 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/f0cd186...e6e9218)      |
| opencontent/ocsensor-ls            | 4.5.2   | 4.5.4   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.5.2...4.5.4)               |
| opencontent/ocsensorapi            | 4.5.0   | 4.5.2   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.0...4.5.2)            |
| opencontent/openpa-ls              | ffa1c9f | 5dc1290 | [...](https://github.com/OpencontentCoop/openpa/compare/ffa1c9f...5dc1290)             |
| php-http/client-common             | ca984d8 | 7c27fe6 | [...](https://github.com/php-http/client-common/compare/ca984d8...7c27fe6)             |
| php-http/discovery                 | 82dbef6 | 7e30d5c | [...](https://github.com/php-http/discovery/compare/82dbef6...7e30d5c)                 |
| php-http/message                   | fcf9b45 | 226ba9f | [...](https://github.com/php-http/message/compare/fcf9b45...226ba9f)                   |
| psr/event-dispatcher               | cfea31e | 3b1c107 | [...](https://github.com/php-fig/event-dispatcher/compare/cfea31e...3b1c107)           |
| symfony/event-dispatcher           | bc8f774 | 8a53fd3 | [...](https://github.com/symfony/event-dispatcher/compare/bc8f774...8a53fd3)           |
| symfony/event-dispatcher-contracts | 5a749bd | 405952c | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5a749bd...405952c) |
| symfony/options-resolver           | 0ab7e5a | f3f9b9c | [...](https://github.com/symfony/options-resolver/compare/0ab7e5a...f3f9b9c)           |
| symfony/polyfill-ctype             | 4719fa9 | e94c8b1 | [...](https://github.com/symfony/polyfill-ctype/compare/4719fa9...e94c8b1)             |
| symfony/polyfill-intl-idn          | 47bd6aa | v1.17.0 | [...](https://github.com/symfony/polyfill-intl-idn/compare/47bd6aa...v1.17.0)          |
| symfony/polyfill-mbstring          | 81ffd3a | fa79b11 | [...](https://github.com/symfony/polyfill-mbstring/compare/81ffd3a...fa79b11)          |
| symfony/polyfill-php72             | 41d115a | f048e61 | [...](https://github.com/symfony/polyfill-php72/compare/41d115a...f048e61)             |
| symfony/polyfill-php73             | 0f27e9f | a760d89 | [...](https://github.com/symfony/polyfill-php73/compare/0f27e9f...a760d89)             |
| symfony/polyfill-php80             | cb64c50 | 5e30b27 | [...](https://github.com/symfony/polyfill-php80/compare/cb64c50...5e30b27)             |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.5.2 and 4.5.4](https://github.com/OpencontentCoop/ocsensor/compare/4.5.2...4.5.4)**
* Migliora la visualizzazione dell'associazione gruppo-categoria
* Migliora la visualizzazione della dashboard operatori suddividendo i post per stato interno
* Permette di rigenerare le immagini avatar svuotando la cache da backend
* Evidenzia maggiormente l'avatar di un utente rimosso

**[opencontent/ocsensorapi changes between 4.5.0 and 4.5.2](https://github.com/OpencontentCoop/ocsensorapi/compare/4.5.0...4.5.2)**
* Avoid duplicated message in same second
* Check approvers consistency

**[opencontent/openpa-ls changes between ffa1c9f and 5dc1290](https://github.com/OpencontentCoop/openpa/compare/ffa1c9f...5dc1290)**
* Corregge un errore nella funzione di template per la ricerca delle strutture
* Corregge un bug in OpenPASMTPTransport


## [2.0.1](https://gitlab.com/opencontent/opensegnalazioni/compare/2.0.0...2.0.1) - 2020-05-07
- Add copy post feature
- Remove solr dev data + fix gitignore

#### Code dependencies
| Changes                        | From    | To      | Compare                                                                             |
|--------------------------------|---------|---------|-------------------------------------------------------------------------------------|
| aws/aws-sdk-php                | 3.134.5 | 3.137.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.134.5...3.137.5)                 |
| composer/installers            | b93bcf0 | 8669edf | [...](https://github.com/composer/installers/compare/b93bcf0...8669edf)             |
| friendsofsymfony/http-cache    | 7560f30 | f0cd186 | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/7560f30...f0cd186)   |
| opencontent/ocfoshttpcache-ls  | aa42fb9 | faa918c | [...](https://github.com/OpencontentCoop/ocfoshttpcache/compare/aa42fb9...faa918c)  |
| opencontent/ocinstaller        | 83adb0a | 7ef45b7 | [...](https://github.com/OpencontentCoop/ocinstaller/compare/83adb0a...7ef45b7)     |
| opencontent/ocopendata-ls      | 2.24.1  | 2.24.2  | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.24.1...2.24.2)        |
| opencontent/ocsensor-ls        | 4.4.2   | 4.5.2   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.4.2...4.5.2)            |
| opencontent/ocsensorapi        | 4.4.0   | 4.5.0   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.4.0...4.5.0)         |
| opencontent/ocsupport-ls       | 49a42aa | 2ee7846 | [...](https://github.com/OpencontentCoop/ocsupport/compare/49a42aa...2ee7846)       |
| opencontent/ocwebhookserver-ls | 32df3cf | aabd35a | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/32df3cf...aabd35a) |
| opencontent/openpa-ls          | 5353d99 | ffa1c9f | [...](https://github.com/OpencontentCoop/openpa/compare/5353d99...ffa1c9f)          |
| psr/simple-cache               | 408d5ea | 5a7b96b | [...](https://github.com/php-fig/simple-cache/compare/408d5ea...5a7b96b)            |
| symfony/event-dispatcher       | d364824 | bc8f774 | [...](https://github.com/symfony/event-dispatcher/compare/d364824...bc8f774)        |
| symfony/options-resolver       | 1cfd933 | 0ab7e5a | [...](https://github.com/symfony/options-resolver/compare/1cfd933...0ab7e5a)        |
| symfony/polyfill-intl-idn      |         | 47bd6aa |                                                                                     |
| symfony/polyfill-php72         |         | 41d115a |                                                                                     |
| symfony/polyfill-php80         | 8854dc8 | cb64c50 | [...](https://github.com/symfony/polyfill-php80/compare/8854dc8...cb64c50)          |


Relevant changes by repository:

**[opencontent/ocfoshttpcache-ls changes between aa42fb9 and faa918c](https://github.com/OpencontentCoop/ocfoshttpcache/compare/aa42fb9...faa918c)**
* Permette di inserire l'id utente nel context hash

**[opencontent/ocinstaller changes between 83adb0a and 7ef45b7](https://github.com/OpencontentCoop/ocinstaller/compare/83adb0a...7ef45b7)**
* Accept custom date() var
* Add remove role assignment Add version compare condition

**[opencontent/ocopendata-ls changes between 2.24.1 and 2.24.2](https://github.com/OpencontentCoop/ocopendata/compare/2.24.1...2.24.2)**
* Corregge l'esposizione di oggetti correlati in default environment qualora essi siano convertiti in formato esteso

**[opencontent/ocsensor-ls changes between 4.4.2 and 4.5.2](https://github.com/OpencontentCoop/ocsensor/compare/4.4.2...4.5.2)**
* Aggiunge il pulsante che permette di duplicare una segnalazione
* Visualizza le segnalazioni correlate nel corpo della segnalazione
* Corregge un problema di generazione delle chiavi di cache per Varnish

**[opencontent/ocsensorapi changes between 4.4.0 and 4.5.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.4.0...4.5.0)**
* Add relatedItems post field

**[opencontent/ocsupport-ls changes between 49a42aa and 2ee7846](https://github.com/OpencontentCoop/ocsupport/compare/49a42aa...2ee7846)**
* Add installer info

**[opencontent/ocwebhookserver-ls changes between 32df3cf and aabd35a](https://github.com/OpencontentCoop/ocwebhookserver/compare/32df3cf...aabd35a)**
* Add simple endpoint api
* Add class definition shared_link
* Fix trigger check, add response to test

**[opencontent/openpa-ls changes between 5353d99 and ffa1c9f](https://github.com/OpencontentCoop/openpa/compare/5353d99...ffa1c9f)**
* Aggiunge il link all'area personale ai valori inseribili nei contatti


## [2.0.0](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.13...2.0.0) - 2020-04-08
- Update docker-compose with ini env vars Update installer Update deps
- Update installer to 1.2.6

#### Code dependencies
| Changes                           | From     | To          | Compare                                                                                |
|-----------------------------------|----------|-------------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                   | 3.133.27 | 3.134.5     | [...](https://github.com/aws/aws-sdk-php/compare/3.133.27...3.134.5)                   |
| composer/installers               | 7d610d5  | b93bcf0     | [...](https://github.com/composer/installers/compare/7d610d5...b93bcf0)                |
| ezsystems/ezpublish-legacy        | 855cc50  | 2020.1000.1 | [...](https://github.com/Opencontent/ezpublish-legacy/compare/855cc50...2020.1000.1)   |
| opencontent/ocbootstrap-ls        | 1.9.6    | 1.9.8       | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.6...1.9.8)            |
| opencontent/ocinstaller           | ce5b6e1  | 83adb0a     | [...](https://github.com/OpencontentCoop/ocinstaller/compare/ce5b6e1...83adb0a)        |
| opencontent/ocmultibinary-ls      | 2.2.1    | 2.2.2       | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.1...2.2.2)          |
| opencontent/ocopendata-ls         | 2.23.5   | 2.24.1      | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.23.5...2.24.1)           |
| opencontent/ocsensor-ls           | 4.3.11   | 4.4.2       | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.11...4.4.2)              |
| opencontent/ocsensorapi           | 4.3.5    | 4.4.0       | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.5...4.4.0)            |
| opencontent/openpa-ls             | 20665ce  | 5353d99     | [...](https://github.com/OpencontentCoop/openpa/compare/20665ce...5353d99)             |
| opencontent/openpa_sensor-ls      | 4.1.4    | 4.2.1       | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.4...4.2.1)          |
| opencontent/openpa_userprofile-ls | 20b44fb  | 2e192b1     | [...](https://github.com/OpencontentCoop/openpa_userprofile/compare/20b44fb...2e192b1) |
| php-http/client-common            | 03086a2  | ca984d8     | [...](https://github.com/php-http/client-common/compare/03086a2...ca984d8)             |
| psr/log                           | e1cb6ec  | 0f73288     | [...](https://github.com/php-fig/log/compare/e1cb6ec...0f73288)                        |
| symfony/event-dispatcher          | 7d4f83e  | d364824     | [...](https://github.com/symfony/event-dispatcher/compare/7d4f83e...d364824)           |
| symfony/options-resolver          | 1942f69  | 1cfd933     | [...](https://github.com/symfony/options-resolver/compare/1942f69...1cfd933)           |
| symfony/polyfill-mbstring         | 766ee47  | 81ffd3a     | [...](https://github.com/symfony/polyfill-mbstring/compare/766ee47...81ffd3a)          |
| symfony/polyfill-php80            |          | 8854dc8     |                                                                                        |
| zetacomponents/console-tools      | 1.7      | 1.7.1       | [...](https://github.com/zetacomponents/ConsoleTools/compare/1.7...1.7.1)              |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.6 and 1.9.8](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.6...1.9.8)**
* Disabilita il drag and drop nell'edit di un attributo ezpage
* Evita in warning in siteaccess di backend per override mancante

**[opencontent/ocinstaller changes between ce5b6e1 and 83adb0a](https://github.com/OpencontentCoop/ocinstaller/compare/ce5b6e1...83adb0a)**
* Add installer type (for submodules)
* Show create/update tag in debug message
* Add sort and priority in content steps Fix var replacer Add sql_copy_from_tsv type Fix bugs
* Bugfixes
* Allow content update Assign role by remote
* Fix dump table

**[opencontent/ocmultibinary-ls changes between 2.2.1 and 2.2.2](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2.1...2.2.2)**
* Permette il download se l'oggetto è in draft e l'utente corrente ne è il proprietario (per preview immagini)

**[opencontent/ocopendata-ls changes between 2.23.5 and 2.24.1](https://github.com/OpencontentCoop/ocopendata/compare/2.23.5...2.24.1)**
* Ottimizza la creazione di sinonimi e di traduzioni di tag via api
* Aggiunge il remote id del main node all'Api Content Metadata
* Permette la creazione e la modifica via api di attributi di tipo ezpage
* Corregge un problema nel calcolo del nodo principale in Metadata
* Merge branch 'ezpage_rest_field'    ezpage_rest_field:   Permette la creazione e la modifica via api di attributi di tipo ezpage
* Corregge lo svuotamento cache del Section repository

**[opencontent/ocsensor-ls changes between 4.3.11 and 4.4.2](https://github.com/OpencontentCoop/ocsensor/compare/4.3.11...4.4.2)**
* Migliora l'interfaccia utente anonimo e autenticato Introduce la possibilità di inserire più immagini Corregge il bug per cui era possibile scaricare un'immagine di una segnalazione privata Introduce il controllo per ip dei permessi di lettura delle api delle immagini Introduce le configurazioni per nascondere la scelta del consenso di pubblicazione e per nascondere la timeline dettagliata Introduce i settings per attivare l'assegnazione casuale dell'operatore di categoria e per la reimpostazione forzata dell'urp a intervento terminato Corregge il termine Assegnatario in Incaricato Corregge l'autenticazione di default nelle api Aggiunge il codice fiscale dell'autore della segnalazione nell'esportazione del csv
* Corregge asset_url nelle clusterizzazioni su aws
* Rimuove le PolicyOmitList di ocsensor in quanto assegnate di default dal ruolo Sensor Anonymous

**[opencontent/ocsensorapi changes between 4.3.5 and 4.4.0](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.5...4.4.0)**
* Avoid warning in category service
* Fix AddApproverAction Use CategoryAutomaticAssignToRandomOperator setting in AddCategoryAction Remove all owners in CloseAction Use ForceUrpApproverOnFix setting in FixAction Use HideTimelineDetails setting in PostService::setUserPostAware Cache groups and operators tree Handle multiple images field in post api Handle fiscal code field in user api Use new api url for file Fix minor bugs Change api version in abb5bf40-077d-42f5-b0fe-079538e6d650

**[opencontent/openpa-ls changes between 20665ce and 5353d99](https://github.com/OpencontentCoop/openpa/compare/20665ce...5353d99)**
* In openpa/object viene considerato il main node
* Aggiunge il codice SDI ai contatti
* Aggiunge la possibilità di escludere alberature dai menu
* Permette la configurazione del mittente email da applicazione
* Rimuovo ruolo e ruolo2 da ContentMain/AbstractIdentifiers  1940
* Considera la variabile d'ambiente EZ_INSTANCE per la definizione dell'identificativo dell'istanza corrente
* Carica la versione del software in CreditsSettings::CodeVersion da file VERSION se presente
* Corregge lo script di migrazione da nfs a s3
* Rimuove codice inutilizzato o obsoleto
* Nella configurazione cluster la cache di ocopendata viene salvata in locale invece che in redis
* Aggiunge TikTok ai possibili contatti
* Considera la variabile d'ambiente EZ_INSTANCE per la definizione dell'identificativo del siteaccess custom per evitare conflitti con i nomi dei moduli

**[opencontent/openpa_sensor-ls changes between 4.1.4 and 4.2.1](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.4...4.2.1)**
* Nasconde editor tools Corregge livello di log in workflow
* Evita la riassegnazione del post all'aggiornamento
* Svuota la cache di operatori e gruppi
* Corregge page_url e asset_url nelle clusterizzazioni su aws

**[opencontent/openpa_userprofile-ls changes between 20b44fb and 2e192b1](https://github.com/OpencontentCoop/openpa_userprofile/compare/20b44fb...2e192b1)**
* Impone il redirect su user/edit


## [1.2.13](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.12...1.2.13) - 2020-03-03
- Update installer to 1.2.5 Add category owner + Fix anonymous role
- Added version file to build phase
- Reduce the building work: get the generic image built for opencontent/ezpublish

#### Code dependencies
| Changes                       | From     | To       | Compare                                                                           |
|-------------------------------|----------|----------|-----------------------------------------------------------------------------------|
| aws/aws-sdk-php               | 3.133.20 | 3.133.27 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.20...3.133.27)             |
| friendsofsymfony/http-cache   | 1658d8a  | 7560f30  | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/1658d8a...7560f30) |
| opencontent/ocbootstrap-ls    | 1.9.4    | 1.9.6    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.4...1.9.6)       |
| opencontent/ocsensor-ls       | 4.3.9    | 4.3.11   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.9...4.3.11)         |
| opencontent/ocsensorapi       | 4.3.4    | 4.3.5    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.4...4.3.5)       |
| opencontent/ocsocialdesign-ls | 1.6.2    | 1.6.3    | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.2...1.6.3)    |
| opencontent/openpa_sensor-ls  | 4.1.1    | 4.1.4    | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.1...4.1.4)     |
| psr/log                       | 5628725  | e1cb6ec  | [...](https://github.com/php-fig/log/compare/5628725...e1cb6ec)                   |
| symfony/event-dispatcher      | ec966bd  | 7d4f83e  | [...](https://github.com/symfony/event-dispatcher/compare/ec966bd...7d4f83e)      |
| symfony/polyfill-ctype        | fbdeaec  | 4719fa9  | [...](https://github.com/symfony/polyfill-ctype/compare/fbdeaec...4719fa9)        |
| symfony/polyfill-mbstring     | 34094cf  | 766ee47  | [...](https://github.com/symfony/polyfill-mbstring/compare/34094cf...766ee47)     |
| symfony/polyfill-php73        | 5e66a0f  | 0f27e9f  | [...](https://github.com/symfony/polyfill-php73/compare/5e66a0f...0f27e9f)        |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.4 and 1.9.6](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.4...1.9.6)**
* Aggiorna le traduzioni in greco
* Aggiorna le traduzioni in greco

**[opencontent/ocsensor-ls changes between 4.3.9 and 4.3.11](https://github.com/OpencontentCoop/ocsensor/compare/4.3.9...4.3.11)**
* Aggiunge expiry per sensor cache
* Corregge alcuni bug del'interfaccia

**[opencontent/ocsensorapi changes between 4.3.4 and 4.3.5](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.4...4.3.5)**
* Intercept category owner

**[opencontent/ocsocialdesign-ls changes between 1.6.2 and 1.6.3](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.2...1.6.3)**
* Permette l'inserimento di link nel testo dei credits

**[opencontent/openpa_sensor-ls changes between 4.1.1 and 4.1.4](https://github.com/OpencontentCoop/openpa_sensor/compare/4.1.1...4.1.4)**
* Aggiunge i template per sensor root e sensor post root
* Aggiunge template di default
* Legge il file VERSION (se presente) per esporre la versione corrente nei credits


## [1.2.12](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.11...1.2.12) - 2020-02-24


#### Code dependencies
| Changes                 | From     | To       | Compare                                                                     |
|-------------------------|----------|----------|-----------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.133.18 | 3.133.20 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.18...3.133.20)       |
| opencontent/ocsensor-ls | 4.3.8    | 4.3.9    | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.8...4.3.9)    |
| opencontent/ocsensorapi | 4.3.3    | 4.3.4    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.3...4.3.4) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.8 and 4.3.9](https://github.com/OpencontentCoop/ocsensor/compare/4.3.8...4.3.9)**
* Corregge un bug di digitazione
* Corregge un bug nell'interfaccia di upload di un allegato

**[opencontent/ocsensorapi changes between 4.3.3 and 4.3.4](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.3...4.3.4)**
* Corregge un'eccezione in caso di inserimento comment in post chiuso
* Fix send notification to group


## [1.2.11](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.10...1.2.11) - 2020-02-20


#### Code dependencies
| Changes                        | From    | To      | Compare                                                                             |
|--------------------------------|---------|---------|-------------------------------------------------------------------------------------|
| opencontent/ocwebhookserver-ls | 38bc369 | 32df3cf | [...](https://github.com/OpencontentCoop/ocwebhookserver/compare/38bc369...32df3cf) |


Relevant changes by repository:

**[opencontent/ocwebhookserver-ls changes between 38bc369 and 32df3cf](https://github.com/OpencontentCoop/ocwebhookserver/compare/38bc369...32df3cf)**
* Add hook filter gui


## [1.2.10](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.9...1.2.10) - 2020-02-20


#### Code dependencies
| Changes                            | From     | To       | Compare                                                                                |
|------------------------------------|----------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.133.13 | 3.133.18 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.13...3.133.18)                  |
| ezsystems/ezpublish-legacy         | 19701b9  | 855cc50  | [...](https://github.com/Opencontent/ezpublish-legacy/compare/19701b9...855cc50)       |
| guzzlehttp/promises                | 6379353  | 89b1a76  | [...](https://github.com/guzzle/promises/compare/6379353...89b1a76)                    |
| opencontent/ocbootstrap-ls         | 1.9.3    | 1.9.4    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.3...1.9.4)            |
| opencontent/ocopendata-ls          | 2.23.4   | 2.23.5   | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.23.4...2.23.5)           |
| opencontent/ocopendata_forms-ls    | 1.6.2    | 1.6.3    | [...](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.2...1.6.3)       |
| opencontent/ocsensor-ls            | 4.3.7    | 4.3.8    | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.7...4.3.8)               |
| opencontent/ocsensorapi            | 4.3.2    | 4.3.3    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.2...4.3.3)            |
| opencontent/ocsocialdesign-ls      | 1.6.1    | 1.6.2    | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.1...1.6.2)         |
| opencontent/openpa-ls              | d3e73a0  | 20665ce  | [...](https://github.com/OpencontentCoop/openpa/compare/d3e73a0...20665ce)             |
| symfony/deprecation-contracts      | f315c0a  | ede224d  | [...](https://github.com/symfony/deprecation-contracts/compare/f315c0a...ede224d)      |
| symfony/event-dispatcher-contracts | 5531ac7  | 5a749bd  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/5531ac7...5a749bd) |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.3 and 1.9.4](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.3...1.9.4)**
* Corregge un bug sulle traduzioni di edit ocevents

**[opencontent/ocopendata-ls changes between 2.23.4 and 2.23.5](https://github.com/OpencontentCoop/ocopendata/compare/2.23.4...2.23.5)**
* Hotfix in SearchGateway query with no limitations

**[opencontent/ocopendata_forms-ls changes between 1.6.2 and 1.6.3](https://github.com/OpencontentCoop/ocopendata_forms/compare/1.6.2...1.6.3)**
* Corregge alcune stringhe di traduzione in greco

**[opencontent/ocsensor-ls changes between 4.3.7 and 4.3.8](https://github.com/OpencontentCoop/ocsensor/compare/4.3.7...4.3.8)**
* Imposta il filtro per categoria sui webhook
* Aggiunge la notifica su messaggio privato
* Corregge un bug dei template nei post senza geolocalizzazione
* Visualizza l'autore del timeline item Corregge il log del webhook

**[opencontent/ocsensorapi changes between 4.3.2 and 4.3.3](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.2...4.3.3)**
* Accept urp as group Add on_send_private_message notification Fix can_respond permission check

**[opencontent/ocsocialdesign-ls changes between 1.6.1 and 1.6.2](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.6.1...1.6.2)**
* Corregge alcune stringhe di traduzione in greco

**[opencontent/openpa-ls changes between d3e73a0 and 20665ce](https://github.com/OpencontentCoop/openpa/compare/d3e73a0...20665ce)**
* Coregge un problema nella creazione dell'oranigramma  2874


## [1.2.9](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.8...1.2.9) - 2020-02-12


#### Code dependencies
| Changes                            | From    | To       | Compare                                                                                |
|------------------------------------|---------|----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.133.7 | 3.133.13 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.7...3.133.13)                   |
| composer/installers                | 71a969f | 7d610d5  | [...](https://github.com/composer/installers/compare/71a969f...7d610d5)                |
| guzzlehttp/promises                | ac2529f | 6379353  | [...](https://github.com/guzzle/promises/compare/ac2529f...6379353)                    |
| opencontent/ocbootstrap-ls         | 1.9.2   | 1.9.3    | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.2...1.9.3)            |
| opencontent/ocmultibinary-ls       | 2.2     | 2.2.1    | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2...2.2.1)            |
| opencontent/ocsensor-ls            | 4.3.5   | 4.3.7    | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.5...4.3.7)               |
| opencontent/ocsensorapi            | 4.3.1   | 4.3.2    | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.1...4.3.2)            |
| opencontent/openpa-ls              | db7c958 | d3e73a0  | [...](https://github.com/OpencontentCoop/openpa/compare/db7c958...d3e73a0)             |
| symfony/deprecation-contracts      |         | f315c0a  |                                                                                        |
| symfony/event-dispatcher           | 6a29d7f | ec966bd  | [...](https://github.com/symfony/event-dispatcher/compare/6a29d7f...ec966bd)           |
| symfony/event-dispatcher-contracts | 454f462 | 5531ac7  | [...](https://github.com/symfony/event-dispatcher-contracts/compare/454f462...5531ac7) |
| symfony/options-resolver           | 188abc9 | 1942f69  | [...](https://github.com/symfony/options-resolver/compare/188abc9...1942f69)           |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.9.2 and 1.9.3](https://github.com/OpencontentCoop/ocbootstrap/compare/1.9.2...1.9.3)**
* Corregge le traduzioni del modulo di cambio password

**[opencontent/ocmultibinary-ls changes between 2.2 and 2.2.1](https://github.com/OpencontentCoop/ocmultibinary/compare/2.2...2.2.1)**
* Fix opendata converter

**[opencontent/ocsensor-ls changes between 4.3.5 and 4.3.7](https://github.com/OpencontentCoop/ocsensor/compare/4.3.5...4.3.7)**
* Fix wrong 204 response
* Corregge un problema nella trasformazione dell'url nel webhook

**[opencontent/ocsensorapi changes between 4.3.1 and 4.3.2](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.1...4.3.2)**
* Bugfix status api
* Fix private message api

**[opencontent/openpa-ls changes between db7c958 and d3e73a0](https://github.com/OpencontentCoop/openpa/compare/db7c958...d3e73a0)**
* Aggiunge WhatsApp e Telegram tra i valori possibili dei contatti
* Considera content_tag_menu nel calcolo del tree menu


## [1.2.8](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.7...1.2.8) - 2020-02-05


#### Code dependencies
| Changes                     | From    | To      | Compare                                                                           |
|-----------------------------|---------|---------|-----------------------------------------------------------------------------------|
| friendsofsymfony/http-cache | 5abb500 | 1658d8a | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/5abb500...1658d8a) |
| opencontent/ocsensorapi     | 4.3.0   | 4.3.1   | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.0...4.3.1)       |


Relevant changes by repository:

**[opencontent/ocsensorapi changes between 4.3.0 and 4.3.1](https://github.com/OpencontentCoop/ocsensorapi/compare/4.3.0...4.3.1)**
* Bugfix openapi definition


## [1.2.7](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.6...1.2.7) - 2020-02-05


#### Code dependencies
| Changes                  | From      | To      | Compare                                                                      |
|--------------------------|-----------|---------|------------------------------------------------------------------------------|
| aws/aws-sdk-php          | 3.133.6   | 3.133.7 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.6...3.133.7)          |
| erasys/openapi-php       | 307cce3   | 9885e8f | [...](https://github.com/erasys/openapi-php/compare/307cce3...9885e8f)       |
| illuminate/contracts     | 5.8.x-dev | 6.x-dev | [...](https://github.com/illuminate/contracts/compare/5.8.x-dev...6.x-dev)   |
| opencontent/ocsensor-ls  | 4.3.4     | 4.3.5   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.4...4.3.5)     |
| symfony/event-dispatcher | af3ef02   | 6a29d7f | [...](https://github.com/symfony/event-dispatcher/compare/af3ef02...6a29d7f) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.4 and 4.3.5](https://github.com/OpencontentCoop/ocsensor/compare/4.3.4...4.3.5)**
* Add spinner in geo search, fix duplicate suggestions, force readonly area select


## [1.2.6](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.5...1.2.6) - 2020-01-31


#### Code dependencies
| Changes                   | From    | To      | Compare                                                                    |
|---------------------------|---------|---------|----------------------------------------------------------------------------|
| aws/aws-sdk-php           | 3.133.5 | 3.133.6 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.5...3.133.6)        |
| opencontent/ocexportas-ls | 1.3.4   | 1.3.5   | [...](https://github.com/OpencontentCoop/ocexportas/compare/1.3.4...1.3.5) |
| opencontent/ocsensor-ls   | 4.3.3   | 4.3.4   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.3...4.3.4)   |
| opencontent/openpa-ls     | 0fffd64 | db7c958 | [...](https://github.com/OpencontentCoop/openpa/compare/0fffd64...db7c958) |


Relevant changes by repository:

**[opencontent/ocexportas-ls changes between 1.3.4 and 1.3.5](https://github.com/OpencontentCoop/ocexportas/compare/1.3.4...1.3.5)**
* Corregge l'esportazione ANAC dei raggruppamenti

**[opencontent/ocsensor-ls changes between 4.3.3 and 4.3.4](https://github.com/OpencontentCoop/ocsensor/compare/4.3.3...4.3.4)**
* Migliora l'esperienza utente in fase di selezione della geolocalizzazione
* Corregge i link da http a https

**[opencontent/openpa-ls changes between 0fffd64 and db7c958](https://github.com/OpencontentCoop/openpa/compare/0fffd64...db7c958)**
* Evoluzione di robots.txt per bloccare i bad crawler


## [1.2.5](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.4...1.2.5) - 2020-01-24


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 4.3.2 | 4.3.3 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.2...4.3.3) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.2 and 4.3.3](https://github.com/OpencontentCoop/ocsensor/compare/4.3.2...4.3.3)**
* Espone la textarea meta anche all'utente con permessi standard


## [1.2.4](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.3...1.2.4) - 2020-01-23


#### Code dependencies
| Changes                 | From  | To    | Compare                                                                  |
|-------------------------|-------|-------|--------------------------------------------------------------------------|
| opencontent/ocsensor-ls | 4.3.1 | 4.3.2 | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.1...4.3.2) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.1 and 4.3.2](https://github.com/OpencontentCoop/ocsensor/compare/4.3.1...4.3.2)**
* hot bugfix


## [1.2.3](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.2...1.2.3) - 2020-01-23
- Fix sensor_post/privacy label in installer

#### Code dependencies
| Changes                 | From    | To      | Compare                                                                  |
|-------------------------|---------|---------|--------------------------------------------------------------------------|
| aws/aws-sdk-php         | 3.133.4 | 3.133.5 | [...](https://github.com/aws/aws-sdk-php/compare/3.133.4...3.133.5)      |
| opencontent/ocsensor-ls | 4.3.0   | 4.3.1   | [...](https://github.com/OpencontentCoop/ocsensor/compare/4.3.0...4.3.1) |


Relevant changes by repository:

**[opencontent/ocsensor-ls changes between 4.3.0 and 4.3.1](https://github.com/OpencontentCoop/ocsensor/compare/4.3.0...4.3.1)**
* Limita la ricerca di nominatim al boundbox del perimetro della prima area configurata


## [1.2.2](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.1...1.2.2) - 2020-01-23


#### Code dependencies
| Changes              | From    | To      | Compare                                                                   |
|----------------------|---------|---------|---------------------------------------------------------------------------|
| opencontent/ocmap-ls | 39ee78b | 93c8432 | [...](https://github.com/OpencontentCoop/ocmap/compare/39ee78b...93c8432) |


Relevant changes by repository:

**[opencontent/ocmap-ls changes between 39ee78b and 93c8432](https://github.com/OpencontentCoop/ocmap/compare/39ee78b...93c8432)**
* Introduce il connettore per ocopendata_form


## [1.2.1](https://gitlab.com/opencontent/opensegnalazioni/compare/1.2.0...1.2.1) - 2020-01-23
- Fix area class in installer



## [1.2.0](https://gitlab.com/opencontent/opensegnalazioni/compare/1.0.0...1.2.0) - 2020-01-22
- Implementazione dei perimetri di area e del servizio di ricerca del civico di prossimità
- Update deps
-  2020-01-13 lorello: required to allow authentication in Sensor Genova      using SIRAC variables comge_nome, comge_cognome, etc...     underscores_in_headers on;
- Update composer dependencies
- Update traefik Docker tag to v1.7.20
- Require user fiscal code as env var
- Assegnazione osservatori in base a Quartiere/Zona di una segnalazione, Predisposizione del modulo di login con SPID, Apertura segnalazioni da chiamate all'Helpdesk (URP)
- Assegnazione osservatori in base a Quartiere/Zona di una segnalazione Predisposizione del modulo di login con SPID Apertura segnalazioni da chiamate all'Helpdesk (URP)
- Add webhooks and fiscal_code validations
- Add zally linter inside CI
- Update sensor extensions, add userprofile in settings, fix installer
- Fix area and category relations classes and editor base role in installer Fix line endings
- Fix docker compose: removed volume on source image
- Changed installer startup: moved inside main entrypoint script to easily use the available variables EZ_ROOT, EZ_INSTANCE
- Fixed unsupported use of variables in Dockefile of ngix
- Fixed Build arg that is now lowercase
- Fixed error on image names
- Fixed variable names using protection parenthesis
- Fixed order of image building: start with php and then build nguinx
- Fixed multiple run when branch=master
- Added multiple image creation
- Dropped zap config
- Added CI
- Update composer
- fix installer sensor admin role
- Update composer
- Update compser, avoid reinstall on socker-compose up
- Upgrade to ocsensor  version 4

#### Code dependencies
| Changes                            | From      | To        | Compare                                                                                |
|------------------------------------|-----------|-----------|----------------------------------------------------------------------------------------|
| aws/aws-sdk-php                    | 3.112.21  | 3.133.4   | [...](https://github.com/aws/aws-sdk-php/compare/3.112.21...3.133.4)                   |
| composer/installers                | 141b272   | 71a969f   | [...](https://github.com/composer/installers/compare/141b272...71a969f)                |
| erasys/openapi-php                 |           | 307cce3   |                                                                                        |
| ezsystems/ezmbpaex-ls              | 5.3.3.1   | 5.3.3.3   | [...](https://github.com/Opencontent/ezmbpaex/compare/5.3.3.1...5.3.3.3)               |
| friendsofsymfony/http-cache        | 01d87fa   | 5abb500   | [...](https://github.com/FriendsOfSymfony/FOSHttpCache/compare/01d87fa...5abb500)      |
| guzzlehttp/guzzle                  | bcbb52f   | 6.5.x-dev | [...](https://github.com/guzzle/guzzle/compare/bcbb52f...6.5.x-dev)                    |
| guzzlehttp/promises                | 17d36ed   | ac2529f   | [...](https://github.com/guzzle/promises/compare/17d36ed...ac2529f)                    |
| illuminate/contracts               |           | 5.8.x-dev |                                                                                        |
| justinrainbow/json-schema          |           | 5.2.9     |                                                                                        |
| league/event                       |           | 7823f10   |                                                                                        |
| mtdowling/jmespath.php             | 2.4.0     | 52168cb   | [...](https://github.com/jmespath/jmespath.php/compare/2.4.0...52168cb)                |
| opencontent/ocbootstrap-ls         | 1.7.4     | 1.9.2     | [...](https://github.com/OpencontentCoop/ocbootstrap/compare/1.7.4...1.9.2)            |
| opencontent/occodicefiscale-ls     |           | 542aa99   |                                                                                        |
| opencontent/ocexportas-ls          | 1.3.3     | 1.3.4     | [...](https://github.com/OpencontentCoop/ocexportas/compare/1.3.3...1.3.4)             |
| opencontent/ocgdprtools-ls         | 1.3.0     | 1.4.5     | [...](https://github.com/OpencontentCoop/ocgdprtools/compare/1.3.0...1.4.5)            |
| opencontent/ocinstaller            | a0f23f7   | ce5b6e1   | [...](https://github.com/OpencontentCoop/ocinstaller/compare/a0f23f7...ce5b6e1)        |
| opencontent/ocmap-ls               |           | 39ee78b   |                                                                                        |
| opencontent/ocmultibinary-ls       | 2.1       | 2.2       | [...](https://github.com/OpencontentCoop/ocmultibinary/compare/2.1...2.2)              |
| opencontent/ocopendata-ls          | 2.22.0    | 2.23.4    | [...](https://github.com/OpencontentCoop/ocopendata/compare/2.22.0...2.23.4)           |
| opencontent/ocopendata_forms-ls    |           | 1.6.2     |                                                                                        |
| opencontent/ocsearchtools-ls       | 1.10.7    | 1.10.9    | [...](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.7...1.10.9)        |
| opencontent/ocsensor-ls            | 3.2.0     | 4.3.0     | [...](https://github.com/OpencontentCoop/ocsensor/compare/3.2.0...4.3.0)               |
| opencontent/ocsensorapi            | 2.1.1     | 4.3.0     | [...](https://github.com/OpencontentCoop/ocsensorapi/compare/2.1.1...4.3.0)            |
| opencontent/ocsiracauth-ls         |           | 1.0       |                                                                                        |
| opencontent/ocsocialdesign-ls      | 1.5.1.0   | 1.6.1     | [...](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.5.1.0...1.6.1)       |
| opencontent/ocsocialuser-ls        | 1.7.2.0   | 1.8.1     | [...](https://github.com/OpencontentCoop/ocsocialuser/compare/1.7.2.0...1.8.1)         |
| opencontent/ocwebhookserver-ls     |           | 38bc369   |                                                                                        |
| opencontent/openpa-ls              | 12d6d5f   | 0fffd64   | [...](https://github.com/OpencontentCoop/openpa/compare/12d6d5f...0fffd64)             |
| opencontent/openpa_sensor-ls       | 1.6       | 4.1.1     | [...](https://github.com/OpencontentCoop/openpa_sensor/compare/1.6...4.1.1)            |
| opencontent/openpa_theme_2014-ls   | 2.14.1    | 2.15.0    | [...](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.14.1...2.15.0)    |
| opencontent/openpa_userprofile-ls  |           | 20b44fb   |                                                                                        |
| php-http/client-common             | 9a2a4c1   | 03086a2   | [...](https://github.com/php-http/client-common/compare/9a2a4c1...03086a2)             |
| php-http/discovery                 | 5a0da02   | 82dbef6   | [...](https://github.com/php-http/discovery/compare/5a0da02...82dbef6)                 |
| php-http/guzzle6-adapter           | 4b491b7   | bd7b405   | [...](https://github.com/php-http/guzzle6-adapter/compare/4b491b7...bd7b405)           |
| php-http/httplug                   | 7af4427   | c9b2424   | [...](https://github.com/php-http/httplug/compare/7af4427...c9b2424)                   |
| php-http/message                   | 144d13b   | fcf9b45   | [...](https://github.com/php-http/message/compare/144d13b...fcf9b45)                   |
| php-http/promise                   | 1cc44dc   | 02ee67f   | [...](https://github.com/php-http/promise/compare/1cc44dc...02ee67f)                   |
| psr/container                      |           | fc1bc36   |                                                                                        |
| psr/event-dispatcher               |           | cfea31e   |                                                                                        |
| psr/log                            | c4421fc   | 5628725   | [...](https://github.com/php-fig/log/compare/c4421fc...5628725)                        |
| psr/simple-cache                   |           | 408d5ea   |                                                                                        |
| symfony/event-dispatcher           | 4.4.x-dev | af3ef02   | [...](https://github.com/symfony/event-dispatcher/compare/4.4.x-dev...af3ef02)         |
| symfony/event-dispatcher-contracts | c43ab68   | 454f462   | [...](https://github.com/symfony/event-dispatcher-contracts/compare/c43ab68...454f462) |
| symfony/options-resolver           | 4.4.x-dev | 188abc9   | [...](https://github.com/symfony/options-resolver/compare/4.4.x-dev...188abc9)         |
| symfony/polyfill-ctype             | 550ebaa   | fbdeaec   | [...](https://github.com/symfony/polyfill-ctype/compare/550ebaa...fbdeaec)             |
| symfony/polyfill-mbstring          |           | 34094cf   |                                                                                        |
| symfony/polyfill-php73             | 2ceb49e   | 5e66a0f   | [...](https://github.com/symfony/polyfill-php73/compare/2ceb49e...5e66a0f)             |
| zetacomponents/mail                | 1.8.3     | 1.9.1     | [...](https://github.com/zetacomponents/Mail/compare/1.8.3...1.9.1)                    |


Relevant changes by repository:

**[opencontent/ocbootstrap-ls changes between 1.7.4 and 1.9.2](https://github.com/OpencontentCoop/ocbootstrap/compare/1.7.4...1.9.2)**
* Specifica la lingua corrente in user/edit, corregge la visualizzazione del template di edit del datatype ocgdpr in bootstrap4
* Recepisce la funzionalità di ordinamento di file multipli in bootstrap4
* Aggiunge il file di traduzione delle stringhe in greco
* Corregge il file di traduzione delle stringhe in greco
* Introduce la funzionalità dei moduli di login per cui è possibile aggiungere un modulo alla pagina /user/login da un'estensione terza
* Permette l'inserimento di testo formattato nei moduli di login
* Recepisce gli aggiornamenti di ocevents

**[opencontent/ocexportas-ls changes between 1.3.3 and 1.3.4](https://github.com/OpencontentCoop/ocexportas/compare/1.3.3...1.3.4)**
* Aggiorna exporter ANAC sulla base di http://www.anticorruzione.it/portal/public/classic/Servizi/ServiziOnline/DichiarazioneAdempLegge190

**[opencontent/ocgdprtools-ls changes between 1.3.0 and 1.4.5](https://github.com/OpencontentCoop/ocgdprtools/compare/1.3.0...1.4.5)**
* Rende traducibile il testo e i link dell'attributo a livello di classe (con retrocompatibilità) Da la possibilità di forzare l'annullamento dell'accettazione e costringe l'utente ad accettare al successivo login Logga su ezaudit
* Bug fix on user acceptance
* Fix conflict with openpa_userprofile
* Corregge la visualizzazione del datatype in versione collectinfo
* Corregge il connettore opendata_form per il caso in cui l'editor non sia l'utente corrente
* Svuota la cache statica (varnish) al cambio di valore di accettazione (varnish mette in cache anche i 302)
* Ignora i privilegi di accesso al modulo gdpr/user_acceptance

**[opencontent/ocinstaller changes between a0f23f7 and ce5b6e1](https://github.com/OpencontentCoop/ocinstaller/compare/a0f23f7...ce5b6e1)**
* Ignore content update, Add custom sql, Typo fixes
* Add storing and checking installer version
* Install tag tree from local yaml
* recursive read tag tree
* Log class compare details
* Add condition parameter
* Get env vars from $_SERVER
* Add is_install_from_scratch global variable
* Bug fix  is_install_from_scratch variable
* workaround is_install_from_scratch and variable not found

**[opencontent/ocmultibinary-ls changes between 2.1 and 2.2](https://github.com/OpencontentCoop/ocmultibinary/compare/2.1...2.2)**
* Added sort feature ( 4)    Added sort feature   Fixed translations

**[opencontent/ocopendata-ls changes between 2.22.0 and 2.23.4](https://github.com/OpencontentCoop/ocopendata/compare/2.22.0...2.23.4)**
* Introduce la ricerca cursor tramite eZFindExtendedAttributeFilterInterface
* Considera più valori per il campo field nel filtro stato
* Permette l'aggiornamento di ezuser senza errore
* Previene fatal error in caso di cluster failure in section repository
* Fix calling static method Base::validate non statically
* Add tag children pagination query parameters
* Fix temp file creation in exportas paginated download

**[opencontent/ocsearchtools-ls changes between 1.10.7 and 1.10.9](https://github.com/OpencontentCoop/ocsearchtools/compare/1.10.7...1.10.9)**
* Corregge l'indicizzazione della prima lettera della stringa e introduce un nuovo subattribute normalized utile per l'ordinamento alfabetico
* Corregge le stringhe statiche e imposta le traduzioni

**[opencontent/ocsensor-ls changes between 3.2.0 and 4.3.0](https://github.com/OpencontentCoop/ocsensor/compare/3.2.0...4.3.0)**
* add admin edit_privacy template avoiding ez error
* edit response action and gui
* Introduce il codice per il google tag manager
* Corregge un bug nella visualizzazione della data di modifica
* Corregge un bug nel form di inserimento delle segnalazioni
* Refactor completo orientato a openapi
* Bugfix
* Bugfix in notifications settings
* Remove user/edit template override
* Bugfix in notifications settings
* Fix mail vars on pre-publish event
* Avoid ezcMvcResponseWriter status code bug
* Fix ApproverCanReopen or AuthorCanReopen
* Add child category in csv export
* Add export in sensor/stat
* Fix ezmpaex forgot password link
* Fix repository::isModerationEnabled()
* Fix category assign action
* Fix terms link ( 8)  Espone il link ai termini di servizio nell'edit di segnalazione https://opencontent.freshdesk.com/a/tickets/2295
* Add full dashboard participant filters
* Merge branch 'refactor-category-assign' into openapi   Conflicts:  	translations/ger-DE/translation.ts  	translations/untranslated/translation.ts
* Fix translations string in edit post
* Add webhooks
* Show edit attribute sensor_root/user_inactivity_days as select
* Introduces the interface for the creation of users when posting
* Introduce new FlashMessage listener Remove unused scenarios
* Recepisce la funzionalità login_modules di ocbootstrap
* Introduce la funzionalità dei perimetri di selezione nelle zone e il servizio opzionale per ricavare il civico di prossimità

**[opencontent/ocsensorapi changes between 2.1.1 and 4.3.0](https://github.com/OpencontentCoop/ocsensorapi/compare/2.1.1...4.3.0)**
* Refactor per esposizione delle api in formato openapi
* Bugfix
* Fix ApproverCanReopen or AuthorCanReopen
* Fix parent/child category in PerCategory stat
* Fix correct object version in PostInitializer
* Unique definition of global moderation in repository::isModerationEnabled
* Fix category assign action
* Consider the reporter and author as distinct users
* Add area observer on set area or on set category Remove unused shenarios Introduce MailValidator as eZMail::validate wrapper Fix some bugs
* Avoid fatal error in FirstAreaApproverScenario when install
* Avoid fatal error in FirstAreaApproverScenario when install
* Add meta attribute and area bounding box in area tree

**[opencontent/ocsocialdesign-ls changes between 1.5.1.0 and 1.6.1](https://github.com/OpencontentCoop/ocsocialdesign/compare/1.5.1.0...1.6.1)**
* Aggiunge il file di traduzione delle stringhe in greco
* Eredita il template di login da ocbootstrap

**[opencontent/ocsocialuser-ls changes between 1.7.2.0 and 1.8.1](https://github.com/OpencontentCoop/ocsocialuser/compare/1.7.2.0...1.8.1)**
* Aggiunge il file di traduzione delle stringhe in greco
* Corregge un bug su stringa di traduzione

**[opencontent/openpa-ls changes between 12d6d5f and 0fffd64](https://github.com/OpencontentCoop/openpa/compare/12d6d5f...0fffd64)**
* Fix create_instance script
* add agenda in OpenPABase::getInstances

**[opencontent/openpa_sensor-ls changes between 1.6 and 4.1.1](https://github.com/OpencontentCoop/openpa_sensor/compare/1.6...4.1.1)**
* Refactor per allineamento alla nuova versione di ocsensor
* Bugfix
* Pass correct object version in PostInitializer
* Add default notification and dashboard filters in workflow
* Add webhook menu
* Hide sensor post in default template

**[opencontent/openpa_theme_2014-ls changes between 2.14.1 and 2.15.0](https://github.com/OpencontentCoop/openpa_theme_2014/compare/2.14.1...2.15.0)**
* Eredita il template di login da ocbootstrap e recepisce la funzionalità login_modules di ocbootstrap


## [1.0.0] - 2019-10-15

First public release


