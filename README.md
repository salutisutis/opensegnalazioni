# OpenSegnalazioni 

[![pipeline status](https://gitlab.com/opencontent/opensegnalazioni/badges/master/pipeline.svg)](https://gitlab.com/opencontent/opensegnalazioni/commits/master)
[![license](https://img.shields.io/badge/license-GPL-blue.svg)](https://gitlab.com/opencontent/opensegnalazioni/blob/master/LICENSE)

OpenSegnalazioni

## Descrizione

...

## Altri riferimenti

Per maggiori informazioni è possibile consultare: 

 * [Demo](https://opensegnalazioni.openpa.opencontent.io)
 * [Manuale utente](https://manuale-opensegnalazioni.readthedocs.io/it/latest/)

## API 

 * [API della Demo](https://opensegnalazioni.openpa.opencontent.io/api/opendata/)
 * [Documentazione API](https://documenter.getpostman.com/view/7046499/S17xqQyU?version=latest)


## Project Status

Il prodotto è *stabile* e *production ready* e usato in produzione in diverse città Italiane. Lo sviluppo avviene in modo costante, sia su richiesta degli Enti utilizzatori, sia su iniziativa autonoma del _maintainer_.

## Informazioni tecniche

L'applicazione è sviluppata sul CMS Ez Publish e consta di codice PHP, 
il dump del database contenente una installazione pienamente funzionante
dell'applicativo e i file di configurazione necessari per il motore di 
ricerca.

### Struttura del Repository

Il repository contiene i seguenti file:
```
composer.json
composer.lock
```
Il repository non contiene direttamente il codice applicativo, contiene 
invece il file delle dipendenze PHP (`composer.json`) che elenca tutti i componenti 
necessari all'applicazione: il CMS Ez Publish, le estensioni dello stesso 
utilizzate e tra queste le estensioni `ocsensor`, `ocsensorapi` e `openpa_sensor` che implementano le funzionalità 
più rilevanti. Con un comando `composer install` dalla stessa directory è possibile ottenere il codice dell'applicativo pronto all'uso (si veda anche [Dockerfile, L3](https://gitlab.com/opencontent/opensegnalazioni/blob/master/Dockerfile#L3))

Le directory
```
/dfs/var/prototipo
/var
```
contengono invece le risorse statiche minime necessarie all'applicazione (loghi, elementi grafici generali, etc...).


Il file `Dockerfile` e le directory
```
/scripts
/conf.d
```
contengono file di supporto per la creazione dell'immagine Docker dell'applicativo.

Il file
```
docker-compose.yml
```
contiene i servizi minimi necessari per l'esecuzione di Comunweb e può essere usato per il deploy su un singolo host o su un cluster Docker Swarm o Kubernetes.

Il file
```
docker-compose.override.yml
```
viene usato automaticamente da docker-compose quando e' disponibile: quando si clona il repository ad esempio e si hanno tutti i file a disposizione, questo aggiunge alcuni servizi di supporto utili in ambiente di sviluppo, come una interfaccia web per il database.


### Requisiti

Lo stack minimo di Comunweb è il seguente:
  * database PostgreSQL 9.6
  * PHP 7.2
  * Solr 4.10.x per il motore di ricerca

Nel file `docker-compose.yml` vengono inoltre utilizzati:
  * Varnish per la cache dei contenuti
  * Traefik per il routing tra i diversi container

Le dipendenze del sistema operativo sono risolte nell'immagine da cui origina
l'immagine di Comunweb, ovvero [opencontentcoop/ezpublish](https://hub.docker.com/r/opencontentcoop/ezpublish). Per i dettagli su questa immagine si rimanda
al [repository](https://www.github.com/OpencontentCoop/docker-ezpublish) ad essa dedicata.

### Come si esegue la build del repository

Con il comando che segue:

    docker build -t opensegnalazioni .

si esegue la build dell'immagine docker dell'applicazione: vengono installate le dipendenze
del sistema operativo e successivamente composer e le dipendenze applicative.

### Local docker environment

Per avere un ambiente completo di demo o di sviluppo clonare
il repository e dalla directory principale dare i comandi:

```
sudo chown 8983 conf.d/solr/prototipo/* -R
docker-compose up -d
```

Il prototipo di Comunweb sarà disponibile ai seguenti indirizzi:

* [opensegnalazioni.localtest.me](https://opensegnalazioni.localtest.me)
* [opensegnalazioni.localtest.me/backend](https://opensegnalazioni.localtest.me/backend), per l'area riservata ai redattori e amministratori (l'utente di default con cui si accede è `admin/change_password`)
* [solr.localtest.me/solr](https://solr.localtest.me/solr), per l'interfaccia amministrativa di SOLR
* [traefik.localtest.me:8080](https://traefik.localtest.me:8080) per l'interfaccia amministrativa di Traefik
* [Mailhog](https://mailhog.opensegnalazioni.localtest.me/), le email inviate in questo ambiente sono visibili in questa interfaccia web
* [PgWeb](https://pgweb.opensegnalazioni.localtest.me/), interfaccia web per il database PostgreSQL

Il certificato usato per tutti i domini è un wildcard self-signed certificate sul dominio *.localtest.me, quindi al primo collegamento si ottiene un warning.

### Useful commands

Per vedere i log di tutti container:

    docker-compose logs -f --tail 20

o solo uno di essi (ad esempio php):

    docker-compose logs -f --tail 20 php

Per ottenere una shell all'interno del container PHP, eseguire:

    docker-compose exec php bash

Per accedere al database da CLI:

    docker-compose exec postgres bash
    bash-4.4# psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB"

Per reindicizzare tutti i contenuti su SOLR:

    docker-compose exec php bash -c 'cd html; php bin/php/updatesearchindex.php -s opensegnalazioni_backend --allow-root-user'

### Rebuild from scratch

Per ricreare l'ambiente originale:

    docker-compose exec php bash -c 'cd html; php vendor/bin/ocinstall --allow-root-user -sprototipo_backend --embed-dfs-schema --no-interaction --cleanup --purge-cache --purge-storage ../installer/'


### Continuous Integration

Il software ha una pipeline di CI, che esegue una build ad ogni commit, disponibile alla seguente url:

https://gitlab.com/opencontent/opensegnalazioni/pipelines

Le build delle immagini docker sono disponibili nel [Registry](https://gitlab.com/opencontent/opensegnalazioni/container_registry) di GitLab.

### Deploy in produzione

Per il deploy in un ambiente di produzione è necessario configurare diversi elementi: in generale
tutte le configurazioni si trovano nei file `conf.d/override/<file>.ini.append.php`, si indicano comunque
di seguito alcuni punti di sicuro interesse. 
In particolare sarà necessario intervenire su:
  * [Dominio](/conf.d/ez/override/site.ini.append.php#L96-97)
  * [Server di posta](/conf.d/ez/override/site.ini.append.php#L103-113)

## Copyright (C)

Il titolare del software è ...

Il software è rilasciato con licenza aperta ai sensi dell'art. 69 comma 1 del [Codice dell’Amministrazione Digitale](https://cad.readthedocs.io/)

### Maintainer

[Opencontent SCARL](https://www.opencontent.it/), è responsabile della progettazione, realizzazione e manutenzione tecnica di Comunweb

