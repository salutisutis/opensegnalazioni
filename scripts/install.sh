#!/usr/bin/env bash

set -e

# Scan for environment variables prefixed with PHP_INI_ENV_ and inject those into ${PHP_INI_DIR}/conf.d/zzz_custom_settings.ini
if [ -f ${PHP_INI_DIR}/conf.d/zzz_custom_settings.ini ]; then rm ${PHP_INI_DIR}/conf.d/zzz_custom_settings.ini; fi
env | while IFS='=' read -r name value ; do
  if (echo $name|grep -E "^PHP_INI_ENV">/dev/null); then
    # remove PHP_INI_ENV_ prefix
    name=`echo $name | cut -f 4- -d "_"`
    echo "[info] SET $name=$value"
    echo $name=$value >> ${PHP_INI_DIR}/conf.d/zzz_custom_settings.ini
  fi
done

if [[ -z $EZ_ROOT ]]; then
    echo "[error] EZ_ROOT is empty this variable is required in this container, please set it to the public dir of Ez and restart"
    exit 1
else
    echo "[info] Current root is ${EZ_ROOT}"
fi

EZ_USER='www-data'
EZ_USER_GROUP='www-data'
EZ_CHMOD_VAR=${EZ_CHMOD_VAR:-'2775'}
EZ_CHMOD_LOG=${EZ_CHMOD_LOG:-'660'}
EZ_CLUSTER_GET_OWNERSHIP=${EZ_CLUSTER_GET_OWNERSHIP:-'true'}

if [ ! -d ${EZ_ROOT}/var/cache/ini ]; then
    mkdir -p ${EZ_ROOT}/var/cache/ini
    echo "[info] created var/cache/ini"
fi

if [ ! -d ${EZ_ROOT}/var/cache/texttoimage ]; then
    mkdir -p ${EZ_ROOT}/var/cache/texttoimage
    echo "[info] created var/cache/texttoimage"
fi

if [ ! -d ${EZ_ROOT}/var/cache/codepages ]; then
    mkdir -p ${EZ_ROOT}/var/cache/codepages
    echo "[info] created var/cache/codepages"
fi

if [ ! -d ${EZ_ROOT}/var/cache/translation ]; then
    mkdir -p ${EZ_ROOT}/var/cache/translation
    echo "[info] created var/cache/translation"
fi

if [ ! -d ${EZ_ROOT}/var/log ]; then
    mkdir -p ${EZ_ROOT}/var/log
    echo "[info] created dir var/log"
fi

for logfile in cluster_error debug error ocfoshttpcache storage warning mugo_varnish_purges strict notice; do
  if [[ ! -f ${EZ_ROOT}/var/log/${logfile}.log ]]; then
    touch ${EZ_ROOT}/var/log/${logfile}.log && \
    chown www-data ${EZ_ROOT}/var/log/${logfile}.log
  fi
  tail -F --pid $$ ${EZ_ROOT}/var/log/${logfile}.log &
done


#Permesso di scrittura per '$EZ_USER_GROUP' per la directory var/
echo "[info] chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var"
      chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var
      chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var/*
      chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var/cache/*
echo "[info] chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var"
      chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var
      chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var/*
      chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var/cache/*

# aumenta la sicurezza dando 660 ai file di log
echo "[info] chmod ${EZ_CHMOD_LOG} ${EZ_ROOT}/var/log/*"
     chmod ${EZ_CHMOD_LOG} ${EZ_ROOT}/var/log/*

echo "[info] chown -R -L $EZ_USER.$EZ_USER_GROUP /var/www/installer"
      chown -R $EZ_USER.$EZ_USER_GROUP /var/www/installer

if [[ -n $EZINI_file__eZDFSClusteringSettings__MountPointPath ]]; then
  if [[ -d $EZINI_file__eZDFSClusteringSettings__MountPointPath ]]; then
    echo "[info] fixing perms in '${EZINI_file__eZDFSClusteringSettings__MountPointPath}' ..."
    chown $EZ_USER $EZINI_file__eZDFSClusteringSettings__MountPointPath
  else
    mkdir -p $EZINI_file__eZDFSClusteringSettings__MountPointPath
    chown $EZ_USER $EZINI_file__eZDFSClusteringSettings__MountPointPath
  fi
fi

RUN_INSTALLER=${RUN_INSTALLER:-'true'}
if [[ -f vendor/bin/ocinstall ]]; then
	if [[ $RUN_INSTALLER == 'true' ]]; then
        if [[ -n $EZ_INSTANCE ]]; then
            echo "[info] run installer on ${EZ_INSTANCE}"
            sudo -E -u $EZ_USER php vendor/bin/ocinstall --allow-root-user -v -sbackend --embed-dfs-schema --no-interaction ../installer/
        fi
  else
      echo "[info] RUN_INSTALLER is set to false"
  fi
else
    echo "[warning] Installer bin vendor/bin/ocinstall not found"
fi

TRUNCATE_CACHE_TABLE=${TRUNCATE_CACHE_TABLE:-'false'}
if [[ $TRUNCATE_CACHE_TABLE == 'true' ]]; then
  echo "[info] truncate cache dfs table"
  php extension/openpa/bin/clustering/truncate_ezdfs_cache.php --allow-root-user
else
  echo "[info] TRUNCATE_CACHE_TABLE is missing or set to false"
fi

REINDEX_SOLR=${REINDEX_SOLR:-'false'}
if [[ $REINDEX_SOLR == 'true' ]]; then
  echo "[info] reindex solr"
  php bin/php/updatesearchindex.php --allow-root-user
else
  echo "[info] REINDEX_SOLR is missing or set to false"
fi

sudo -E -u $EZ_USER php bin/php/ezcache.php --clear-all

exec "$@"