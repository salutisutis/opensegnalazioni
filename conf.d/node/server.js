const express = require('express')
const app = express()
const http = require('http').createServer(app);
const crypto = require('crypto');
const io = require('socket.io')(http, {
    cors: {
        origin: process.env.APP_URL || "https://opensegnalazioni.localtest.me",
        methods: ["GET", "POST"]
    }
});
var redis = require('socket.io-redis');
const port = process.env.PORT || 3000;
const secret = process.env.SECRET || 'abcdefghi'
const redisEndpoint = process.env.REDIS_ENDPOINT || false;

if (redisEndpoint){
    const redisEndpointPort = process.env.REDIS_ENDPOINT_PORT || 6379;
    io.adapter(redis({ host: redisEndpoint, port: redisEndpointPort }));
}

app.use(express.json());
app.post('/', (req, res) => {
    let data = req.body;
    let requestJson = JSON.stringify(data);
    let signature = req.header('Signature');
    let token = crypto.createHmac("sha256", secret).update(requestJson).digest().toString('base64');
    if (signature === token) {
        if (typeof data === 'object'
            && data.hasOwnProperty('identifier')
            && data.hasOwnProperty('data')
        ) {
            console.log('OK: Receive valid data ' + data.identifier);
            io.emit(data.identifier, data.data);
            res.sendStatus(200);
        }else{
            console.log('ERROR: Invalid data');
            res.sendStatus(400);
        }
    } else {
        console.log('ERROR: Invalid signature: ' + signature + ' (expected: ' + token + ')');
        res.sendStatus(403);
    }
});

io.on('connection', (socket) => {
    console.log('-> connected');
    socket.on('disconnect', function () {
        console.log('<- disconnected');
    });
    socket.on('broadcast', function (data) {
        console.log(data);
        if (typeof data === 'object'
            && data.hasOwnProperty('identifier')
            && data.hasOwnProperty('data')
        ) {
            io.emit(data.identifier, data.data);
        }
    });
});

http.listen(port, () => {
    console.log('listening on *:' + port);
});